use std::ops::Div;

/// A normal distribution.
#[derive(Clone, Debug)]
pub struct NormalDist {
    /// The mean of the distribution
    pub mu : f64,

    /// The standard deviation
    pub sigma : f64,
}

impl NormalDist {
    pub fn from_data(data : Vec<f64>) -> Self {
        let mean : f64 = data.iter()
            .fold(0.0, |acc, x| acc + x)
            .div(data.len() as f64);

        let std_dev : f64 = data.iter()
            .fold(0.0, |acc, x| acc + (x - mean).powi(2))
            .div(data.len() as f64)
            .sqrt();

        NormalDist { mu: mean, sigma: std_dev }
    }

    /// Get the difference between the mean and the given value
    /// in terms of the standard deviation.
    pub fn test(&self, val : f64) -> f64 {
        (val - self.mu) / self.sigma
    }
}

