use std::time::Instant;
use std::net::TcpStream;
use ssh2::Session;

/// Attempt to connect the given host with the given username
/// and password. Expects a connection failure and returns the
/// time it took for the connection to fail.
pub fn attempt_connect(target : &str, user : &str, pass : &str) -> f64 {
    let tcp = TcpStream::connect(target).unwrap();
    let mut ssh = Session::new().unwrap();
    ssh.handshake(&tcp).unwrap();
    
    let timer = Instant::now();

    ssh.userauth_password(user, pass);

    let dur = timer.elapsed();
    dur.as_secs() as f64 + (dur.subsec_millis() as f64 / 1000.0)
}


