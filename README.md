This repo contains little projects I wrote to get familiar with the Rust programming language:

- `primes`: a prime number calculator, capable of listing prime numbers, testing a number's primality and extracting the prime factors of a number.

- `happy-numbers`: a happy numbers calculator.

- `chem-balance`: an unfinished and not working chemical equation balancer.

- `tetris`: a simplistic clone of Tetris.

- `sshenum`: an exploit for CVE-2016-6210.

- `ftpd`: a toy FTP server.

- `naive-mlp`: a naive implementation of a multilayer perceptron for recognizing MNIST handwritten digits.

- `rt-crack`: a hash cracker based on rainbow tables.

