//! This module contains functions to read the files of the MNIST dataset

use algebra::Vector;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use itertools::Itertools;

/// Represents a digit from the MNIST data set.
#[derive(Debug, Clone)]
pub struct MNISTDigit {
    /// The image of the digit as a 784-dimensional vector
    pub image : Vector,

    /// The expected result vector, as a 10-dimensional vector
    pub expected : Vector,
}

/// Extract all the digits from a file in the dataset
pub fn mnist_read_file(images_file : &Path, labels_file : &Path) -> Vec<MNISTDigit> {
    let images_it = File::open(images_file).unwrap()
        .bytes()
        .skip(16)
        .map(|m_byte| m_byte.unwrap() as f32 / 255.0)
        .chunks(28 * 28);

    let labels_it = File::open(labels_file).unwrap()
        .bytes()
        .skip(8)
        .map(|m_byte| {
            let mut l = vec![0f32; 10];
            l[m_byte.unwrap() as usize] = 1.0;
            l
        });

    labels_it.zip(images_it.into_iter())
        .map(|(l, i)| MNISTDigit {
            expected : Vector::from_vec(l),
            image : Vector::from_vec(i.collect())
        })
        .collect()
}


