//! This module contains abstractions that make manipulating neural networks
//! and their layers more convenient

use algebra::{Vector, Matrix};
use std::ops::{Add, Mul, Sub};
 

/// The activation function for neurons
fn sigmoid(x : f32) -> f32 {
    1.0 / (1.0 + (-x).exp())
}

/// The derivative of the sigmoid
fn sigmoid_prime(x : f32) -> f32 {
    sigmoid(x) * (1.0 - sigmoid(x))
}


/// Represents a layer in a neural network with its weights and biases.
#[derive(Debug, Clone, PartialEq)]
pub struct Layer {
    pub weights : Matrix,
    pub biases : Vector,
}

impl Layer {
    /// Create a new layer with all parameters set to the same value.
    pub fn new_with_val(val : f32, prev_size : usize, size : usize) -> Self {
        Layer {
            weights : Matrix::from_val(val, prev_size, size),
            biases : Vector::from_val(val, size),
        }
    }

    /// Create a new layer with all parameters set at random.
    pub fn new_random(prev_size : usize, size : usize) -> Self {
        Layer {
            weights : Matrix::random(prev_size, size),
            biases : Vector::random(size),
        }
    }

    /// Get the activation of the layer before application of the activation
    /// function of the given input.
    pub fn activation_pre(&self, v: &Vector) -> Vector {
        &(&self.weights * v) + &self.biases
    }

    /// Get the activation of the layer for a given input
    pub fn activation(&self, v : &Vector) -> Vector {
        self.activation_pre(v)
            .map_components(sigmoid)
    }
}

/// Adding the weights and biases of two layers
impl<'a, 'b> Add<&'b Layer> for &'a Layer {
    type Output = Layer;

    fn add(self, other : &'b Layer) -> Layer {
        Layer {
            weights : &self.weights + &other.weights,
            biases : &self.biases + &other.biases,
        }
    }
}

/// Subtracting the weights and biases of two layers
impl<'a, 'b> Sub<&'b Layer> for &'a Layer {
    type Output = Layer;

    fn sub(self, other : &'b Layer) -> Layer {
        Layer {
            weights : &self.weights - &other.weights,
            biases : &self.biases - &other.biases,
        }
    }
}

/// Multiplying the weights and the biases of a layer by a scalar.
impl<'a> Mul<f32> for &'a Layer {
    type Output = Layer;

    fn mul(self, lambda : f32) -> Layer {
        Layer {
            weights : &self.weights * lambda,
            biases : &self.biases * lambda,
        }
    }
}


/// Represents a full neural network
#[derive(Debug, Clone, PartialEq)]
pub struct Net {
    pub layers : Vec<Layer>,
}

impl Net {
    /// Create a net with all parameters set to the same value
    pub fn new_with_val(val : f32, layer_sizes : &Vec<usize>) -> Self {
        let mut sizes_it = layer_sizes.into_iter();
        if let Some(first_sz) = sizes_it.next() {
            let mut prev_sz = first_sz;
            let mut res = Vec::new();

            for curr_sz in sizes_it {
                res.push(Layer::new_with_val(val, *prev_sz, *curr_sz));
                prev_sz = curr_sz;
            }

            Net { layers : res }
        
        } else { Net { layers : Vec::new() } }
    }

    /// Create a net with all parameters set at random
    pub fn new_random(layer_sizes : &Vec<usize>) -> Self {
        // FIXME: DRY
        let mut sizes_it = layer_sizes.into_iter();
        if let Some(first_sz) = sizes_it.next() {
            let mut prev_sz = first_sz;
            let mut res = Vec::new();

            for curr_sz in sizes_it {
                res.push(Layer::new_random(*prev_sz, *curr_sz));
                prev_sz = curr_sz;
            }

            Net { layers : res }
        
        } else { Net { layers : Vec::new() } }
    }

    /// Run the MLP with the given input vector. The dimension of the vector
    /// must be the same as the number of neurons in the input layer.
    pub fn forward(&self, input : &Vector) -> Vector {
        self.layers.iter()
            .fold(input.clone(), |v, l| l.activation(&v))
    }

    /// Get the activations of all the layers for a given input. Returns two
    /// Vecs of vectors containing respectively :
    ///   - The activations of the neurons of each layer before application
    ///     of the activation function.
    ///   - The activations of the neurons of each layer after application of
    ///     the activation function.
    fn activations(&self, input : &Vector) -> (Vec<Vector>, Vec<Vector>) {
        let mut acs_pre = Vec::with_capacity(self.layers.len());
        acs_pre.push(input.clone());
        let mut acs_post = Vec::with_capacity(self.layers.len());
        acs_post.push(input.clone());
        let mut ac_post = input.clone();

        for layer in self.layers.iter() {
            let ac_pre = layer.activation_pre(&ac_post);
            ac_post = ac_pre.map_components(sigmoid);
            
            acs_pre.push(ac_pre);
            acs_post.push(ac_post.clone());
        }
        
        (acs_pre, acs_post)
    }

    /// Use backpropagation to calculate the error function. It is returned as
    /// a Vec of Vectors that represent the error in each neuron of each layer
    fn backpropagate(&self, acs_pre : &Vec<Vector>, 
                            acs_post : &Vec<Vector>,
                            expected : &Vector) -> Vec<Vector> {
        // FIXME: This whole function could be way better.
        let mut err_f = Vec::with_capacity(self.layers.len());

        // Error function for the output layer
        let mut next_err = (acs_post.last().unwrap() - expected)
            .hadamard(&acs_pre.last().unwrap().map_components(sigmoid_prime));
        err_f.push(next_err.clone());

        // Error function for the other layers
        for i in (0..(self.layers.len() - 1)).rev() {
            let weights = &self.layers[i + 1].weights;
            let err = (&weights.transpose() * &next_err)
                // The index is i+1 because the activations take the input layer
                // into account while the layers vec does not, which mean that
                // each element of acs_pre is shifted by one position relative
                // to the corresponding layer in self.layers. It's ugly. FIXME
                .hadamard(&acs_pre[i + 1].map_components(sigmoid_prime));

            err_f.push(err.clone());
            next_err = err;
        }

        err_f.reverse();
        err_f
    }

    /// Calculate the adjustments for learning
    pub fn deltas(&self, input : &Vector, expected : &Vector) -> Net {
        // Calculate the error function though backpropagation
        let (acs_pre, acs_post) = self.activations(input);
        let err_f = self.backpropagate(&acs_pre, &acs_post, &expected);

        // Calculate the deltas from the error function
        let mut delta_layers = Vec::with_capacity(self.layers.len());
        for i in 1..acs_post.len() {
            // The index in err_f is also i - 1 because of the same
            // ugliness. FIXME
            delta_layers.push(Layer {
                weights : &err_f[i - 1] * &acs_post[i - 1],
                biases : err_f[i - 1].clone()
            });
        }

        Net { layers : delta_layers }
    }
}

/// Adding the parameters of two networks together
impl<'a, 'b> Add<&'b Net> for &'a Net {
    type Output = Net;

    fn add(self, other : &'b Net) -> Net {
        if self.layers.len() != other.layers.len() { panic!("Adding networks of different depths"); }

        let res = self.layers.iter().zip(other.layers.iter())
            .map(|(a, b)| a + b)
            .collect();
        Net { layers : res }
    }
}

/// Subtracting the parameters of two networks
impl<'a, 'b> Sub<&'b Net> for &'a Net {
    type Output = Net;

    fn sub(self, other : &'b Net) -> Net {
        if self.layers.len() != other.layers.len() { panic!("Sub-ing networks of different depths"); }

        let res = self.layers.iter().zip(other.layers.iter())
            .map(|(a, b)| a - b)
            .collect();
        Net { layers : res }
    }
}

/// Multiplying the parameters of a network by a scalar
impl<'a> Mul<f32> for &'a Net {
    type Output = Net;

    fn mul(self, lambda : f32) -> Net {
        let res = self.layers.iter()
            .map(|x| x * lambda)
            .collect();
        Net { layers : res }
    }
}


#[cfg(test)]
mod tests {
    use algebra::{Vector, Matrix};
    use neural::net::{Layer, Net};

    #[test]
    fn layer_new_with_val_test() {
        let layer = Layer::new_with_val(1.0, 3, 5);

        assert_eq!(layer.biases, Vector::from_vec(vec![1.0; 5]));
        assert_eq!(layer.weights, Matrix::from_val(1.0, 3, 5));
    }

    #[test]
    fn layer_new_random_test() {
        let layer = Layer::new_random(3, 5);

        assert_eq!(layer.biases.dimension(), 5);
        assert_eq!(layer.weights.height(), 5);
        assert_eq!(layer.weights.width(), 3);
    }

    #[test]
    fn layer_activation_pre_test() {
        let layer = Layer::new_with_val(2.0, 3, 5);
        let input = Vector::from_vec(vec![1.0; 3]);

        let res = layer.activation_pre(&input);
        let expected = Vector::from_vec(vec![8.0; 5]);
    }

    #[test]
    fn layer_addition_test() {
        let layer_1 = Layer::new_with_val(1.0, 3, 5);
        let layer_2 = Layer::new_with_val(2.0, 3, 5);
        let expected = Layer::new_with_val(3.0, 3, 5);

        let res = &layer_1 + &layer_2;

        assert_eq!(res, expected);
    }

    #[test]
    fn layer_subtraction_test() {
        let layer_1 = Layer::new_with_val(1.0, 3, 5);
        let layer_2 = Layer::new_with_val(2.0, 3, 5);
        let expected = Layer::new_with_val(-1.0, 3, 5);

        let res = &layer_1 - &layer_2;

        assert_eq!(res, expected);
    }

    #[test]
    fn layer_scalar_multiplication_test() {
        let res = &Layer::new_with_val(1.0, 3, 5) * 3.0;
        let expected = Layer::new_with_val(3.0, 3, 5);

        assert_eq!(res, expected);
    }

    #[test]
    fn net_new_with_val_test() {
        let net = Net::new_with_val(2.0, &vec![3, 5, 2]);

        assert_eq!(net.layers.len(), 2);
        assert_eq!(net.layers[0].biases.dimension(), 5);
        assert_eq!(net.layers[1].biases.dimension(), 2);
    }

    #[test]
    fn net_new_random_test() {
        let net = Net::new_random(&vec![3, 5, 2]);

        assert_eq!(net.layers.len(), 2);
        assert_eq!(net.layers[0].biases.dimension(), 5);
        assert_eq!(net.layers[1].biases.dimension(), 2);
    }

    #[test]
    fn net_forward_test() {
        let net = Net::new_with_val(2.0, &vec![3, 5, 2]);
        let res = net.forward(&Vector::from_vec(vec![1.0; 3]));

        assert_eq!(res.dimension(), 2);
    }

    #[test]
    fn net_activations_test() {
        let net = Net::new_with_val(2.0, &vec![3, 5, 2]);
        let (res_pre, res_post) = net.activations(&Vector::from_vec(vec![1.0; 3]));

        assert_eq!(res_pre.len(), 3);
        assert_eq!(res_post.len(), 3);

        assert_eq!(res_pre[0].dimension(), 3);
        assert_eq!(res_post[0].dimension(), 3);

        assert_eq!(res_pre[1].dimension(), 5);
        assert_eq!(res_post[1].dimension(), 5);

        assert_eq!(res_pre[2].dimension(), 2);
        assert_eq!(res_post[2].dimension(), 2);
    }

    #[test]
    fn net_addition_test() {
        let net_1 = Net::new_with_val(1.0, &vec![3, 5, 2]);
        let net_2 = Net::new_with_val(2.0, &vec![3, 5, 2]);

        let expected = Net::new_with_val(3.0, &vec![3, 5, 2]);
        let res = &net_1 + &net_2;

        assert_eq!(expected, res);
    }

    #[test]
    fn net_subtraction_test() {
        let net_1 = Net::new_with_val(1.0, &vec![3, 5, 2]);
        let net_2 = Net::new_with_val(2.0, &vec![3, 5, 2]);

        let expected = Net::new_with_val(-1.0, &vec![3, 5, 2]);
        let res = &net_1 - &net_2;

        assert_eq!(expected, res);
    }

    #[test]
    fn net_scalar_multiplication_test() {
        let res = &Net::new_with_val(2.0, &vec![3, 5, 2]) * 3.0;
        let expected = Net::new_with_val(6.0, &vec![3, 5, 2]);

        assert_eq!(expected, res);
    }

    #[test]
    fn net_backpropagation_test() {
        let net = Net::new_random(&vec![3, 5, 2]);
        let (acs_pre, acs_post) = net.activations(&Vector::from_vec(vec![3.0; 3]));
        let res = net.backpropagate(&acs_pre, &acs_post, &Vector::from_vec(vec![3.0; 2]));

        assert_eq!(res.len(), 2);
        assert_eq!(res[0].dimension(), 5);
        assert_eq!(res[1].dimension(), 2);
    }

    #[test]
    fn net_deltas_test() {
        let net = Net::new_random(&vec![3, 5, 2]);
        let input = Vector::from_vec(vec![3.0; 3]);
        let res = net.deltas(&input, &Vector::from_vec(vec![3.0; 2]));

        assert_eq!(res.layers.len(), 2);

        assert_eq!(res.layers[0].weights.width(), 3);
        assert_eq!(res.layers[0].weights.height(), 5);
        assert_eq!(res.layers[0].biases.dimension(), 5);

        assert_eq!(res.layers[1].weights.width(), 5);
        assert_eq!(res.layers[1].weights.height(), 2);
        assert_eq!(res.layers[1].biases.dimension(), 2);
    }

    #[test]
    fn net_backpropagation_deep_test() {
        let net = Net::new_random(&vec![3, 5, 7, 9, 2]);
        let (acs_pre, acs_post) = net.activations(&Vector::from_vec(vec![3.0; 3]));
        let res = net.backpropagate(&acs_pre, &acs_post, &Vector::from_vec(vec![3.0; 2]));

        assert_eq!(res.len(), 4);
        assert_eq!(res[0].dimension(), 5);
        assert_eq!(res[1].dimension(), 7);
        assert_eq!(res[2].dimension(), 9);
        assert_eq!(res[3].dimension(), 2);
    }

    #[test]
    fn net_deltas_deep_test() {
        let net = Net::new_random(&vec![3, 5, 7, 9, 2]);
        let input = Vector::from_vec(vec![3.0; 3]);
        let res = net.deltas(&input, &Vector::from_vec(vec![3.0; 2]));

        assert_eq!(res.layers.len(), 4);

        assert_eq!(res.layers[0].weights.width(), 3);
        assert_eq!(res.layers[0].weights.height(), 5);
        assert_eq!(res.layers[0].biases.dimension(), 5);

        assert_eq!(res.layers[1].weights.width(), 5);
        assert_eq!(res.layers[1].weights.height(), 7);
        assert_eq!(res.layers[1].biases.dimension(), 7);

        assert_eq!(res.layers[2].weights.width(), 7);
        assert_eq!(res.layers[2].weights.height(), 9);
        assert_eq!(res.layers[2].biases.dimension(), 9);

        assert_eq!(res.layers[3].weights.width(), 9);
        assert_eq!(res.layers[3].weights.height(), 2);
        assert_eq!(res.layers[3].biases.dimension(), 2);
    }
}

