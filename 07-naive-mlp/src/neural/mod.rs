//! This module contains high-level training and testing functions. The actual
//! 'neural network' code is in neural::net.

use mnist::MNISTDigit;

mod net;
use neural::net::Net;

/// Represents a multilayer perceptron. Each element is a pair of a matrix of
/// weights and a vector of biases.
#[derive(Debug, Clone)]
pub struct MLPerceptron {
    net : Net,
    sizes : Vec<usize>,
}

impl MLPerceptron {
    /// Create a new multilayer perceptron with the given layers, each element
    /// of the Vec representing the number of neurons in the layer. The weights
    /// and biases are initialized randomly.
    pub fn with_layers(layer_sizes : Vec<usize>) -> Self {
        MLPerceptron {
            net : Net::new_random(&layer_sizes),
            sizes : layer_sizes,
        }
    }

    /// Train the perceptron for a mini_batch
    fn train_mini_batch(&mut self, batch : &[MNISTDigit], eta : f32) {
        let null_l = Net::new_with_val(0.0, &self.sizes);
        let delta = batch.iter()
            .fold(null_l, |d, s| &d + &self.net.deltas(&s.image, &s.expected));
        self.net = &self.net - &(&delta * eta);
    }

    /// Train the perceptron through one epoch with the given training data,
    /// learning rate, and batch size
    pub fn train_epoch(&mut self, set : &Vec<MNISTDigit>, eta : f32, batch_sz : usize) {
        for batch in set.as_slice().chunks(batch_sz) {
            self.train_mini_batch(batch, eta / batch_sz as f32);
        }
    }

    /// Test the perceptron with the given test data and return the number of
    /// correct classifications
    pub fn test(&self, set : &Vec<MNISTDigit>) -> u32 {
        set.iter().fold(0, |sum, sample| {
                let got_result = self.net.forward(&sample.image).max_index();
                let expected = sample.expected.max_index();

                if got_result == expected { sum + 1 } else { sum }
            })
    }
} 



