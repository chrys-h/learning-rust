//! This module implements matrices

use std::iter::{Iterator, repeat};
use std::ops::{Add, Sub, Mul};
use rand::{thread_rng, Rng};

use algebra::vector::*;

/// Represents an n*m matrix of numbers of the given type.
#[derive(Debug, Clone, PartialEq)]
pub struct Matrix {
    /// The contents of the matrix.
    conts : Vec<f32>,

    /// The dimensions of the matrix
    height : usize,
    width : usize,
}

impl Matrix {
    /// Create a Matrix by repeating the given value.
    pub fn from_val(val : f32, w : usize, h : usize) -> Self {
        Matrix {
            conts : repeat(val).take(w * h).collect(),
            width : w,
            height : h,
        }
    }

    /// Create a new matrix with components at random between -3.0 and 3.0
    pub fn random(width : usize, height : usize) -> Self {
        let total_count = width * height;
        let mut vec = Vec::with_capacity(total_count);
        for _ in 0..total_count {
            vec.push(thread_rng().gen_range(-3.0f32, 3.0f32));
        }

        Matrix {
            conts : vec,
            width : width,
            height : height,
        }
    }

    /// Get the height of a matrix
    pub fn height(&self) -> usize {
        self.height
    }

    /// Get the width of a matrix
    pub fn width(&self) -> usize {
        self.width
    }

    /// Get an iterator over the rows of the matrix
    pub fn rows(&self) -> MatrixRows {
        MatrixRows::new(self)
    }

    /// Get an iterator over the columns of the matrix
    pub fn columns(&self) -> MatrixCols {
        MatrixCols::new(self)
    }
    
    /// Create a matrix from a rows iterator
    pub fn from_rows<I>(mut rows_it : I) -> Self where I: Iterator<Item = Vector> {
        if let Some(first) = rows_it.next() {
            let mut mat = Matrix {
                width : first.dimension(),
                height : 1,
                conts : first.contents().clone(),
            };

            for v in rows_it {
                if v.dimension() != mat.width {
                    panic!("Creating matrix from vectors of different dimensions");
                }

                mat.height += 1;
                mat.conts.append(&mut v.contents().clone());
            }

            mat
        } else {
            Matrix {
                width : 0,
                height : 0,
                conts : Vec::new(),
            }
        }
    }

    ///// Create a matrix from a columns iterator
    //pub fn from_columns<I>(rows_it : I) -> Self where I: Iterator<Vector> {
    //}         // TODO IF NEEDED
    
    /// Transpose a matrix
    pub fn transpose(&self) -> Self {
        Matrix::from_rows(self.columns())
    }
}

/// Matrix addition
impl<'a, 'b> Add<&'b Matrix> for &'a Matrix {
    type Output = Matrix;

    fn add(self, other : &'b Matrix) -> Matrix {
        if self.height != other.height || self.width != other.width {
            panic!("Adding matrices of different sizes");
        }

        let res_rows = self.rows()
            .zip(other.rows())
            .map(|(u, v)| &u + &v);

        Matrix::from_rows(res_rows)
    }
}

/// Matrix subtraction
impl<'a, 'b> Sub<&'b Matrix> for &'a Matrix {
    type Output = Matrix;

    fn sub(self, other : &'b Matrix) -> Matrix {
        if self.height != other.height || self.width != other.width {
            panic!("Subtracting matrices of different sizes");
        }

        let res_rows = self.rows()
            .zip(other.rows())
            .map(|(u, v)| &u - &v);

        Matrix::from_rows(res_rows)
    }
}

/// Outer product of vectors
impl<'a, 'b> Mul<&'b Vector> for &'a Vector {
    type Output = Matrix;

    fn mul(self, other : &'b Vector) -> Matrix {
        let rows = self.iter()
            .map(|x| other * x);
        Matrix::from_rows(rows)
    }
}

/// Vector multiplication
impl<'a, 'b> Mul<&'b Vector> for &'a Matrix {
    type Output = Vector;

    fn mul(self, vector : &'b Vector) -> Vector {
        if self.width != vector.dimension() {
            panic!("Multiplying a matrix and a vector of incompatible sizes");
        }

        let new_rows = self.rows()
            .map(|r| r.hadamard(vector));
        let new_mat = Matrix::from_rows(new_rows);

        let null_vector = Vector::from_val(0.0, self.height);
        new_mat.columns()
            .fold(null_vector, |acc, v| &acc + &v)
    }
}

/// Scalar multiplication
impl<'a> Mul<f32> for &'a Matrix {
    type Output = Matrix;

    fn mul(self, lambda : f32) -> Matrix {
        let res_conts = self.conts.iter()
            .map(|x| x * lambda)
            .collect();

        Matrix {
            width : self.width,
            height : self.height,
            conts : res_conts,
        }
    }
}


/// An iterator over the rows of a matrix
#[derive(Debug, Clone)]
pub struct MatrixRows<'a> {
    /// The matrix iterated over
    matrix : &'a Matrix,

    /// The position of the iterator
    pos : usize,
}

impl<'a> MatrixRows<'a> {
    /// Create a new iterator
    pub fn new(mat : &'a Matrix) -> Self {
        MatrixRows {
            matrix : mat,
            pos : 0,
        }
    }
}

impl<'a> Iterator for MatrixRows<'a> {
    type Item = Vector;

    fn next(&mut self) -> Option<Self::Item> {
        let res = if self.pos < self.matrix.height {
            let pos = self.pos * self.matrix.width;
            let slice = &self.matrix.conts[pos..pos + self.matrix.width];
            Some(Vector::from_vec(slice.to_vec()))

        } else {
            None
        };

        self.pos += 1;
        res
    }
}


/// An iterator over the columns of a matrix
#[derive(Debug, Clone)]
pub struct MatrixCols<'a> {
    /// The matrix iterated over
    matrix : &'a Matrix,

    /// The position of the iterator
    pos : usize,
}

impl<'a> MatrixCols<'a> {
    /// Create a new iterator
    pub fn new(mat : &'a Matrix) -> Self {
        MatrixCols {
            matrix : mat,
            pos : 0,
        }
    }
}

impl<'a> Iterator for MatrixCols<'a> {
    type Item = Vector;

    fn next(&mut self) -> Option<Self::Item> {
        let res = if self.pos < self.matrix.width {
            let mut vec = Vec::with_capacity(self.matrix.height);

            for i in 0..self.matrix.height {
                let index = self.pos + i * self.matrix.width;
                vec.push(self.matrix.conts[index]);
            }

            Some(Vector::from_vec(vec))

        } else {
            None
        };

        self.pos += 1;
        res
    }
}


#[cfg(test)]
mod tests {
    use algebra::matrix::Matrix;
    use algebra::vector::Vector;

    #[test]
    fn from_rows_test() {
        let r1 = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let r2 = Vector::from_vec(vec![2.0, 3.0, 4.0]);
        let res = Matrix::from_rows(vec![r1, r2].into_iter());

        assert_eq!(res.width, 3);
        assert_eq!(res.height, 2);
        assert_eq!(res.conts, vec![1.0, 2.0, 3.0, 
                                   2.0, 3.0, 4.0]);
    }

    #[test]
    fn from_val_test() {
        let res = Matrix::from_val(2.0, 3, 2);

        assert_eq!(res.width, 3);
        assert_eq!(res.height, 2);
        assert_eq!(res.conts, vec![2.0, 2.0, 2.0,
                                   2.0, 2.0, 2.0]);
    }

    #[test]
    fn addition_test() {
        let r1 = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let r2 = Vector::from_vec(vec![2.0, 3.0, 4.0]);
        let m1 = Matrix::from_rows(vec![r1, r2].into_iter());

        let r3 = Vector::from_vec(vec![3.0, 4.0, 1.0]);
        let r4 = Vector::from_vec(vec![2.0, 1.0, 5.0]);
        let m2 = Matrix::from_rows(vec![r3, r4].into_iter());

        let res = &m1 + &m2;

        assert_eq!(res.width, 3);
        assert_eq!(res.height, 2);
        assert_eq!(res.conts, vec![4.0, 6.0, 4.0,
                                   4.0, 4.0, 9.0]);
    }

    #[test]
    fn subtraction_test() {
        let r1 = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let r2 = Vector::from_vec(vec![2.0, 3.0, 4.0]);
        let m1 = Matrix::from_rows(vec![r1, r2].into_iter());

        let r3 = Vector::from_vec(vec![3.0, 4.0, 1.0]);
        let r4 = Vector::from_vec(vec![2.0, 1.0, 5.0]);
        let m2 = Matrix::from_rows(vec![r3, r4].into_iter());

        let res = &m1 - &m2;

        assert_eq!(res.width, 3);
        assert_eq!(res.height, 2);
        assert_eq!(res.conts, vec![-2.0, -2.0, 2.0,
                                   0.0,  2.0,  -1.0]);
    }

    #[test]
    fn outer_product_test() {
        let u = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let v = Vector::from_vec(vec![4.0, 5.0]);

        let res = &u * &v;

        assert_eq!(res.width, 2);
        assert_eq!(res.height, 3);
        assert_eq!(res.conts, vec![4.0,  5.0,
                                   8.0,  10.0,
                                   12.0, 15.0]);
    }

    #[test]
    fn vector_multiplication_test() {
        let r1 = Vector::from_vec(vec![1.0, 2.0]);
        let r2 = Vector::from_vec(vec![3.0, 4.0]);
        let r3 = Vector::from_vec(vec![5.0, 6.0]);
        let r4 = Vector::from_vec(vec![7.0, 8.0]);
        let m1 = Matrix::from_rows(vec![r1, r2, r3, r4].into_iter());

        let v = Vector::from_vec(vec![1.0, 2.0]);

        let res = &m1 * &v;

        assert_eq!(res.dimension(), 4);
        assert_eq!(res.contents().clone(), vec![5.0, 11.0, 17.0, 23.0]);
    }

    #[test]
    fn scalar_multiplication_test() {
        let r1 = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let r2 = Vector::from_vec(vec![2.0, 3.0, 4.0]);
        let m1 = Matrix::from_rows(vec![r1, r2].into_iter());

        let res = &m1 * 2.0;

        assert_eq!(res.width, 3);
        assert_eq!(res.height, 2);
        assert_eq!(res.conts, vec![2.0, 4.0, 6.0,
                                   4.0, 6.0, 8.0]);
    }

    #[test]
    fn transpose_test() {
        let r1 = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let r2 = Vector::from_vec(vec![2.0, 3.0, 4.0]);
        let m1 = Matrix::from_rows(vec![r1, r2].into_iter());

        let res = m1.transpose();

        assert_eq!(res.width, 2);
        assert_eq!(res.height, 3);
        assert_eq!(res.conts, vec![1.0, 2.0,
                                   2.0, 3.0,
                                   3.0, 4.0]);
    }
}


