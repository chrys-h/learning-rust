//! This module contains an implementation of vectors.

use std::iter::{Iterator, repeat};
use std::ops::{Add, Sub, Mul};
use rand::{thread_rng, Rng};
use std::cmp::Ordering::Equal;


/// Represents an n-dimensional vector of number of the given type.
#[derive(Debug, Clone, PartialEq)]
pub struct Vector {
    /// The contents of the vector
    conts : Vec<f32>,

    /// The dimension of the vector
    dim : usize,
}

impl Vector {
    // TODO: Implement FromIter and IntoIter

    /// Create a new vector by repeating the same value.
    pub fn from_val(val : f32, dim : usize) -> Self {
        let vec_res = repeat(val)
            .take(dim)
            .collect();
        Self::from_vec(vec_res)
    }

    /// Create a new vector with components at random between -3.0 and 3.0
    pub fn random(dim : usize) -> Self {
        let mut vec = Vec::with_capacity(dim);
        for _ in 0..dim {
            vec.push(thread_rng().gen_range(-3.0f32, 3.0f32));
        }

        Self::from_vec(vec)
    }

    /// Create a new vector from a Vec
    pub fn from_vec(vec : Vec<f32>) -> Self {
        Vector {
            dim : vec.len(),
            conts : vec
        }
    }

    /// Get the dimension of a vector
    pub fn dimension(&self) -> usize {
        self.dim
    }

    /// Get the contents of a vector
    pub fn contents(&self) -> &Vec<f32> {
        &self.conts
    }

    /// Create an iterator over the components of the vector.
    pub fn iter(&self) -> VectorIter {
        VectorIter::new(self)
    }

    /// Apply a function over all the components of the vector and return
    /// the resulting vector. Really just a shorthand for map.
    pub fn map_components<F>(&self, f : F) -> Vector where F: Fn(f32) -> f32 {
        let res = self.iter().map(f).collect();
        Self::from_vec(res)
    }

    /// Get the Hadamard product of two vectors
    pub fn hadamard(&self, other : &Self) -> Self {
        let res = self.iter().zip(other.iter())
            .map(|(a, b)| a * b)
            .collect();
        Self::from_vec(res)
    }

    /// Get the index of the biggest component
    pub fn max_index(&self) -> usize {
        self.iter().enumerate()
            .max_by(|&a, &b| a.1.partial_cmp(&b.1).unwrap_or(Equal))
            .unwrap().0
    }
}

impl<'a, 'b> Add<&'b Vector> for &'a Vector {
    type Output = Vector;

    fn add(self, other : &'b Vector) -> Vector {
        if self.dim != other.dim { panic!("Adding different sized vectors"); };

        let res_conts = self.iter().zip(other.iter())
            .map(|(a, b)| a + b)
            .collect();

        Vector {
            conts : res_conts,
            dim : self.dim,
        }
    }
}

impl<'a, 'b> Sub<&'b Vector> for &'a Vector {
    type Output = Vector;

    fn sub(self, other : &'b Vector) -> Vector {
        if self.dim != other.dim { panic!("Subtracting different sized vectors"); };
        
        let res_conts = self.iter().zip(other.iter())
            .map(|(a, b)| a - b)
            .collect();

        Vector {
            conts : res_conts,
            dim : self.dim,
        }
    }
}

/// Implementation of multiplication by a scalar.
impl<'a> Mul<f32> for &'a Vector {
    type Output = Vector;

    fn mul(self, scalar : f32) -> Vector {
        let res_conts = self.iter()
            .map(|a| a * scalar)
            .collect();

        Vector {
            conts : res_conts,
            dim : self.dim,
        }
    }
}

/// Iterator over the values of a Vector
#[derive(Debug, Clone)]
pub struct VectorIter<'a> {
    /// The vector
    vector : &'a Vector,

    /// The current position of the iterator
    pos : usize,
}

impl<'a> VectorIter<'a> {
    /// Create a new iterator.
    pub fn new(v : &'a Vector) -> Self {
        VectorIter {
            vector : v,
            pos : 0,
        }
    }
}

impl<'a> Iterator for VectorIter<'a> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let res = if self.pos < self.vector.dim {
            Some(self.vector.conts[self.pos])
        } else {
            None
        };

        self.pos += 1;
        res
    }
}

#[cfg(test)]
mod tests {
    use algebra::vector::Vector;

    #[test]
    fn from_vec_test() {
        let vec = vec![1.0, 2.0, 3.0];
        let res = Vector::from_vec(vec.clone());
        
        assert_eq!(res.dim, 3);
        assert_eq!(res.conts, vec);
    }

    #[test]
    fn constant_val_test() {
        let res = Vector::from_val(2.0, 3);

        assert_eq!(res.dim, 3);
        assert_eq!(res.conts, vec![2.0, 2.0, 2.0]);
    }

    #[test]
    fn map_components_test() {
        let res = Vector::from_vec(vec![1.0, 2.0, 3.0])
            .map_components(|x| x - 1.0);

        assert_eq!(res.dim, 3);
        assert_eq!(res.conts, vec![0.0, 1.0, 2.0]);
    }

    #[test]
    fn hadamard_test() {
        let a = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let b = Vector::from_vec(vec![2.0, 2.0, 2.0]);
        let res = a.hadamard(&b);

        assert_eq!(res.dim, 3);
        assert_eq!(res.conts, vec![2.0, 4.0, 6.0]);
    }

    #[test]
    fn max_index_test() {
        let res = Vector::from_vec(vec![1.0, 2.0, 3.0]).max_index();
        assert_eq!(res, 2);
    }

    #[test]
    fn addition_test() {
        let a = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let b = Vector::from_vec(vec![2.0, 2.0, 2.0]);
        let res = &a + &b;

        assert_eq!(res.dim, 3);
        assert_eq!(res.conts, vec![3.0, 4.0, 5.0]);
    }

    #[test]
    fn subtraction_test() {
        let a = Vector::from_vec(vec![1.0, 2.0, 3.0]);
        let b = Vector::from_vec(vec![2.0, 2.0, 2.0]);
        let res = &a - &b;

        assert_eq!(res.dim, 3);
        assert_eq!(res.conts, vec![-1.0, 0.0, 1.0]);
    }

    #[test]
    fn multiplication_test() {
        let res = &Vector::from_vec(vec![1.0, 2.0, 3.0]) * 2.0;

        assert_eq!(res.dim, 3);
        assert_eq!(res.conts, vec![2.0, 4.0, 6.0]);
    }
}



