//! This module contains linear algebra and vector calculus primitives.

mod vector;
pub use self::vector::*;
mod matrix;
pub use self::matrix::*;




