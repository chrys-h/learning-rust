#![feature(duration_extras)]

extern crate rand;
extern crate itertools;
extern crate clap;

mod algebra;
mod neural;
mod mnist;

use neural::MLPerceptron;
use mnist::*;
use clap::{Arg, App};
use rand::{thread_rng, Rng};
use std::path::Path;
use std::io::stdout;
use std::io::Write;
use std::time::Instant;

fn main() { // FIXME: Getting awfully long
    // Parsing CLI arguments
    let matches = App::new("Naive MLP test")
        .version("1.0")
        .author("Chrysostome Huchet")
        .about("A naive implementation of a multilayer perceptron")
        .arg(Arg::with_name("learning-rate")
             .short("r")
             .long("learning-rate")
             .value_name("LEARNING RATE")
             .takes_value(true))
        .arg(Arg::with_name("batch-size")
             .short("b")
             .long("batch-size")
             .value_name("BATCH SIZE")
             .takes_value(true))
        .arg(Arg::with_name("hidden-layers")
             .short("l")
             .long("hidden-layers")
             .value_name("HIDDEN LAYERS")
             .required(true)
             .takes_value(true))
        .arg(Arg::with_name("exponential-factor")
             .short("e")
             .long("exponential-factor")
             .value_name("FACTOR")
             .takes_value(true))
        .get_matches();

    let mut learning_rate = matches.value_of("learning-rate")
        .unwrap_or("0.1")
        .parse::<f32>()
        .unwrap();

    let batch_size = matches.value_of("batch-size")
        .unwrap_or("20")
        .parse::<usize>()
        .unwrap();

    let exponential_factor : Option<f32> = matches.value_of("exponential-factor")
        .map(|s| s.parse::<f32>().unwrap());

    let mut layers = matches.value_of("hidden-layers")
        .unwrap()
        .split(",")
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    layers.push(10);
    layers.insert(0, 28 * 28);


    // Opening data sets
    println!("Loading data sets from mnist/...");
    let mnist_testing = mnist_read_file(Path::new("mnist/test_images"),
                                        Path::new("mnist/test_labels"));
    let mut mnist_training = mnist_read_file(Path::new("mnist/train_images"),
                                             Path::new("mnist/train_labels"));


    // Creating perceptron
    println!("Initializing perceptron with layer sizes {:?}...", layers);
    let mut mlp = MLPerceptron::with_layers(layers);


    // Main training loop
    println!("Training perceptron...");
    for epoch_no in 1.. {
        print!("Epoch no. {}", epoch_no); stdout().flush();
        let timer = Instant::now();

        // Run one training epoch
        thread_rng().shuffle(&mut mnist_training);
        mlp.train_epoch(&mnist_training, learning_rate, batch_size);

        // Choose whether to continue
        let success = (mlp.test(&mnist_testing) as f32 * 100.0) / mnist_testing.len() as f32;
        let dur = timer.elapsed();
        let elapsed = dur.as_secs() as f64 + (dur.subsec_millis() as f64 / 1000.0);
        println!(" : {:.3}% success ({:.2} seconds)", success, elapsed);
        if success > 95.0 { break; }

        // Apply the exponential factor if any
        if let Some(lambda) = exponential_factor {
            learning_rate *= lambda;
        }
    }

    println!("SUCCESS");
}



