#!/usr/bin/env bash

mkdir -p mnist/

wget yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
wget yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
wget yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
wget yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz

gunzip *.gz

mv train-images-idx3-ubyte mnist/train_images
mv train-labels-idx1-ubyte mnist/train_labels
mv t10k-images-idx3-ubyte  mnist/test_images
mv t10k-labels-idx1-ubyte  mnist/test_labels

