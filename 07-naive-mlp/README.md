# Naive multilayer perceptron

This project implements an artifcial neural network from scratch, starting with linear algebra functions (in the `algebra` module) and up to the actual neural network functions (in the `neural` module). The ANN is trained to recognize handwritten digits from the MNIST dataset.

## Usage

Before running the program, obtain the MNIST set by running
```
$ ./get_mnist.sh
```

As usual, the program can be compiled with
```
$ cargo build
```

and the unit tests can be ran with
```
$ cargo test
```

The script downloads the dataset and extracts the archives into `mnist/`.

The basic usage of the program is:
```
$ cargo run --release -- --hidden-layers <SIZE>,<SIZE>,<...>
```

The output and input layers will be added automatically.

Additionally, the size of minibatches can be set with the `-b` option, the learning rate with `-r` and a factor that the learning rate will be multiplied by after each epoch with `-e`.

The program automatically stops when the success rate has reached 95%. This can be changed by modifying the relevant line towards the end of `main()` in `main.rs`.

