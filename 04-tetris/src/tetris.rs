//! Tetris game data structures and logic
use rand;

/// Direction of a step, based on user input
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum StepDir {
    /// Move the current block left.
    MoveLeft,

    /// Move the current block right.
    MoveRight,

    /// Move the current block down.
    MoveDown,

    /// Rotate the current block.
    Rotate,

    /// Drop the current block.
    DropBlock,
}

/// Colors of individual boxes in the grid.
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum TetrisBox {
    /// For I-shaped blocks
    Cyan,

    /// For O-shaped blocks
    Yellow,

    /// For T-shaped blocks
    Purple,

    /// For S-shaped blocks
    Green,

    /// For Z-shaped blocks
    Red,

    /// For J-shaped blocks
    Blue,

    /// For L-shaped blocks
    Orange,
}

pub type TetrisMatrix = Vec<Vec<Option<TetrisBox>>>;
pub type TetrisCoords = (i32, i32);

/// State of a game of Tetris.
#[derive(Debug)]
pub struct TetrisGame {
    /// Matrix of the blocks that are set, ordered from the bottom up and
    /// from left to right.
    pub matrix_set : TetrisMatrix,

    /// Matrix of the blocks that are moving, ordered from the bottom up
    /// and from left to right.
    pub matrix_current : TetrisMatrix,

    /// Current position of the bottommost, leftmost square (whether it
    /// contains a block or not) of the currently moving matrix.
    pub position_current : TetrisCoords,

    /// Upcoming matrix of blocks, ordered from the bottom up and left to
    /// right.
    pub matrix_next : TetrisMatrix,

    /// Position to spawn the next blocks.
    pub position_next : TetrisCoords,

    /// Interval between ticks
    pub ticks_interval : f64,

    /// If the game is over
    pub game_over : bool,
}

impl TetrisGame {
    /// Create a new game with default state.
    pub fn new() -> Self {
        let (curr_mat, curr_pos) = get_random_block();
        let (next_mat, next_pos) = get_random_block();

        TetrisGame {
            matrix_set : vec![vec![None ; 10] ; 22],
            matrix_current : curr_mat,
            position_current : curr_pos,
            matrix_next : next_mat,
            position_next : next_pos,
            ticks_interval : 1.2f64,
            game_over : false,
        }
    }

    /// Test the legality of a given position of the current block.
    fn position_legal(&self, new_curr : &TetrisMatrix, &(new_x, new_y) : &TetrisCoords) -> bool {
        if new_x >= 0 && new_x + (new_curr[0].len() as i32) <= (self.matrix_set[0].len() as i32)
        && new_y >= 0 && new_y + (new_curr.len() as i32) <= (self.matrix_set.len() as i32) {
            for i in 0..new_curr.len() {
                for j in 0..new_curr[i].len() {
                    let set_i = i + new_y as usize;
                    let set_j = j + new_x as usize;
                    if self.matrix_set[set_i][set_j].is_some()
                    && new_curr[i][j].is_some() {
                        return false;
                    }
                }
            }

            true
        } else {
            false
        }
    }

    /// Find the lowest possible position for the current block.
    pub fn lowest_pos(&self) -> TetrisCoords {
        let mut lowest = self.position_current.clone();
        while self.position_legal(&self.matrix_current, &(lowest.0, lowest.1 - 1)) {
            lowest.1 -= 1;
        }

        lowest
    }

    /// Delete a row and move all superior rows down.
    fn delete_row(&mut self, row_num : usize) {
        let last_row : usize = self.matrix_set.len() - 1;
        for row in row_num..last_row {
            self.matrix_set[row] = self.matrix_set[row + 1].clone();
        }
        self.matrix_set[last_row] = vec![None; 10];
    }

    /// Check for completed lines and delete them.
    fn check_lines(&mut self) {
        let mut row : i32 = self.matrix_set.len() as i32 - 1;
        let mut changed = false;
        while row >= 0 {
            let completed = self.matrix_set[row as usize].iter()
                .all(|o| o.is_some());
            if completed {
                changed = true;
                self.delete_row(row as usize);
            }
            row -= 1;
        }

        if changed {
            self.ticks_interval *= 0.9;
        }
    }

    /// Fix the current block.
    fn fix_current(&mut self) {
        for row in 0..self.matrix_current.len() {
            for col in 0..self.matrix_current[row].len() {
                let abs_row = row + self.position_current.1 as usize;
                let abs_col = col + self.position_current.0 as usize;
                let curr = self.matrix_current[row][col].clone();
                if curr.is_some() {
                    self.matrix_set[abs_row][abs_col] = curr;
                }
            }
        }
    }

    /// Fix the current block and set up the next.
    fn next_block(&mut self) {
        self.fix_current();
        self.check_lines();
        self.matrix_current = self.matrix_next.clone();
        self.position_current = self.position_next.clone();

        if self.matrix_set[19].iter().any(|b| b.is_some()) {
            self.game_over = true;
        } else {
            let (new_mat, new_pos) = get_random_block();
            self.matrix_next = new_mat;
            self.position_next = new_pos;
        }
    }

    /// Test the legality of a given move and applies it if possible.
    fn try_move(&mut self, new_mat : TetrisMatrix, new_pos : TetrisCoords) {
        if self.position_legal(&new_mat, &new_pos) {
            self.matrix_current = new_mat;
            self.position_current = new_pos;

            if self.position_current == self.lowest_pos() {
                self.next_block();
            }
        }
    }

    /// Compute the result of one step in the game.
    pub fn step(&mut self, dir : StepDir) {
        match dir {
            StepDir::MoveLeft => {
                let (x, y) = self.position_current.clone();
                let mat = self.matrix_current.clone();
                self.try_move(mat, (x - 1, y));
            }
            StepDir::MoveRight => {
                let (x, y) = self.position_current.clone();
                let mat = self.matrix_current.clone();
                self.try_move(mat, (x + 1, y));
            }
            StepDir::Rotate => {
                let pos = self.position_current.clone();
                let mat = rotate_matrix(&self.matrix_current);
                self.try_move(mat, pos);
            }
            StepDir::MoveDown => {
                let (x, y) = self.position_current.clone();
                let mat = self.matrix_current.clone();
                self.try_move(mat, (x, y - 1));
            }
            StepDir::DropBlock => {
                let pos = self.lowest_pos();
                let mat = self.matrix_current.clone();
                self.try_move(mat, pos);
            }
        }
    }
}

fn rotate_matrix(mat : &TetrisMatrix) -> TetrisMatrix {
    let mut res : TetrisMatrix = Vec::new();
    for _ in 0..mat[0].len() {
        let mut row = Vec::new();
        for _ in 0..mat.len() {
            row.push(None);
        }
        res.push(row);
    }

    for i in 0..mat[0].len() {
        for j in 0..mat.len() {
            res[i][j] = mat[mat.len() - j - 1][i].clone();
        }
    }

    res
}

fn get_random_block() -> (TetrisMatrix, TetrisCoords) {
    match rand::random::<u32>() % 7 {
        0 => block_i(),
        1 => block_o(),
        2 => block_t(),
        3 => block_s(),
        4 => block_z(),
        5 => block_j(),
        6 => block_l(),
        _ => panic!("Invalid random number")
    }
}

fn block_i() -> (TetrisMatrix, TetrisCoords) {
    let some = Some(TetrisBox::Cyan);
    let pos = (3, 20);
    let lower = vec![some.clone(), some.clone(), some.clone(), some.clone()];
    let mat = vec![lower];
    
    (mat, pos)
}

fn block_o() -> (TetrisMatrix, TetrisCoords) {
    let some = Some(TetrisBox::Yellow);
    let pos = (4, 20);
    let upper = vec![some.clone(), some.clone()];
    let lower = vec![some.clone(), some.clone()];
    let mat = vec![lower, upper];
    
    (mat, pos)
}

fn block_t() -> (TetrisMatrix, TetrisCoords) {
    let some = Some(TetrisBox::Purple);
    let pos = (3, 20);
    let upper = vec![None, some.clone(), None];
    let lower = vec![some.clone(), some.clone(), some.clone()];
    let mat = vec![lower, upper];
    
    (mat, pos)
}

fn block_s() -> (TetrisMatrix, TetrisCoords) {
    let some = Some(TetrisBox::Green);
    let pos = (3, 20);
    let upper = vec![None, some.clone(), some.clone()];
    let lower = vec![some.clone(), some.clone(), None];
    let mat = vec![lower, upper];
    
    (mat, pos)
}

fn block_z() -> (TetrisMatrix, TetrisCoords) {
    let some = Some(TetrisBox::Red);
    let pos = (3, 20);
    let upper = vec![some.clone(), some.clone(), None];
    let lower = vec![None, some.clone(), some.clone()];
    let mat = vec![lower, upper];
    
    (mat, pos)
}

fn block_j() -> (TetrisMatrix, TetrisCoords) {
    let some = Some(TetrisBox::Blue);
    let pos = (3, 20);
    let upper = vec![None, None, some.clone()];
    let lower = vec![some.clone(), some.clone(), some.clone()];
    let mat = vec![lower, upper];
    
    (mat, pos)
}

fn block_l() -> (TetrisMatrix, TetrisCoords) {
    let some = Some(TetrisBox::Orange);
    let pos = (3, 20);
    let upper = vec![some.clone(), None, None];
    let lower = vec![some.clone(), some.clone(), some.clone()];
    let mat = vec![lower, upper];
    
    (mat, pos)
}


