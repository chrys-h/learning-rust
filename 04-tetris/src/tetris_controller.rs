use piston::input::{GenericEvent, Key, Button};
use tetris::{TetrisGame, StepDir};

/// A Tetris game controller
pub struct TetrisController {
    /// The state of the game
    pub game : TetrisGame,

    /// Current time into the game
    pub time_now : f64,
    
    /// Time as of the last tick
    pub time_prev : f64,
}

impl TetrisController {
    /// Create a new tetris controller
    pub fn new(g : TetrisGame) -> Self {
        TetrisController {
            game : g,
            time_now : 0f64,
            time_prev : 0f64,
        }
    }

    /// Handle an event.
    pub fn handle_event<E: GenericEvent>(&mut self, evt : &E) {
        if ! self.game.game_over {
            if let Some(Button::Keyboard(key)) = evt.press_args() {
                match key {
                    Key::Up    => self.game.step(StepDir::Rotate),
                    Key::Down  => self.game.step(StepDir::MoveDown),
                    Key::Left  => self.game.step(StepDir::MoveLeft),
                    Key::Right => self.game.step(StepDir::MoveRight),
                    Key::Space => self.game.step(StepDir::DropBlock),
                    _ => {}
                }
            }

            if let Some(update) = evt.update_args() {
                self.time_now += update.dt;
                if self.time_now - self.time_prev > self.game.ticks_interval {
                    self.game.step(StepDir::MoveDown);
                    self.time_prev = self.time_now;
                }
            }
        }
	}
}


