extern crate rand;
extern crate piston;
extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;

use piston::window::WindowSettings;
use glutin_window::GlutinWindow;
use piston::event_loop::{Events, EventSettings};
use piston::input::RenderEvent;
use opengl_graphics::{OpenGL, GlGraphics};

mod tetris;
mod tetris_view;
mod tetris_controller;

use tetris::TetrisGame;
use tetris_view::{TetrisViewSettings, TetrisView};
use tetris_controller::TetrisController;

fn main() {
	let ogl_v = OpenGL::V3_2;
    let mut window : GlutinWindow = WindowSettings::new("My Tetris", [800, 600])
		.opengl(ogl_v)
        .exit_on_esc(true)
        .build()
        .expect("Could not create window");

    let mut events = Events::new(EventSettings::new());
	let mut gl = GlGraphics::new(ogl_v);

    let mut tetris_ctlr = TetrisController::new(TetrisGame::new());
    let tetris_view = TetrisView::new(TetrisViewSettings::new());

    while let Some(e) = events.next(&mut window) {
        tetris_ctlr.handle_event(&e);

		if let Some(args) = e.render_args() {
			gl.draw(args.viewport(), |c, g| {
				use graphics::{clear};

				clear([1.0; 4], g);
                tetris_view.draw(&tetris_ctlr, &c, g);
			});
		}
    }
}


