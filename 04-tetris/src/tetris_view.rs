use tetris::{TetrisBox, TetrisMatrix, TetrisCoords};
use tetris_controller::{TetrisController};

use std::collections::HashMap;
use graphics::types::Color;
use graphics::{Context, Graphics, Rectangle};
use graphics::color::hex;

type TetrisCols = HashMap<TetrisBox, Color>;

/// Settings for a tetris view.
pub struct TetrisViewSettings {
    main_position : [f64; 2],
    upcoming_position : [f64; 2],
    box_size : f64,
    box_colors : TetrisCols,
    game_over_col : Color,
}

impl TetrisViewSettings {
    /// Create new view settings with default values.
    pub fn new() -> Self {
        let box_sz = 50.0;
        let mut box_cols : HashMap<TetrisBox, Color> = HashMap::with_capacity(7);
        box_cols.insert(TetrisBox::Cyan,   hex("46f0f0"));
        box_cols.insert(TetrisBox::Yellow, hex("ffe119"));
        box_cols.insert(TetrisBox::Purple, hex("911eb4"));
        box_cols.insert(TetrisBox::Green,  hex("3cb44b"));
        box_cols.insert(TetrisBox::Red,    hex("e6194b"));
        box_cols.insert(TetrisBox::Blue,   hex("0082c8"));
        box_cols.insert(TetrisBox::Orange, hex("f58232"));

        TetrisViewSettings {
            main_position : [10.0; 2],
            upcoming_position : [ 20.0 + 10.0 * box_sz, 10.0 ],
            box_size : box_sz,
            box_colors : box_cols,
            game_over_col : hex("000000")
        }
    }
}

/// A view for a game of tetris.
pub struct TetrisView {
    /// The settings of the view
    settings : TetrisViewSettings,
}

impl TetrisView {
    /// Create a new tetris view.
    pub fn new(s : TetrisViewSettings) -> Self {
        TetrisView {
            settings: s
        }
    }

    /// Draw a matrix of bricks
    fn draw_matrix<G:Graphics>(&self,
                               mat : &TetrisMatrix,
                               pos : &[f64; 2],
                               cols : &TetrisCols,
                               c : &Context,
                               g : &mut G ) {
        let h = if mat.len() > 21 { 21 } else { mat.len() }; // HACK
        let w = mat[0].len();
        for row_no in 0..h {
            for col_no in 0..w {
                if let Some(ref curr_box) = mat[row_no][col_no] {
                    let sz = self.settings.box_size;
                    let y = pos[1] + (h - row_no) as f64 * sz;
                    let x = pos[0] + col_no as f64 * sz;
                    let rect = [x, y, sz, sz];

                    Rectangle::new(cols[curr_box])
                        .draw(rect, &c.draw_state, c.transform, g);
                }
            }
        }
    }

    /// Draw a matrix of bricks within an already-drawn matrix of bricks.
    fn overlay_matrix<G:Graphics>(&self,
                                  outer : &TetrisMatrix,
                                  outer_pos : &[f64; 2],
                                  inner : &TetrisMatrix,
                                  &(inner_x, inner_y) : &TetrisCoords,
                                  inner_alpha : f64, 
                                  c : &Context, 
                                  g : &mut G ) {
        let mut layer : TetrisMatrix = Vec::new();
        let inner_x_end = inner_x as usize + inner[0].len();
        let inner_y_end = inner_y as usize + inner.len();

        for row_no in 0..outer.len() {
            let mut row = Vec::new();

            for col_no in 0..outer[row_no].len() {
                if row_no >= inner_y as usize && row_no < inner_y_end as usize
                && col_no >= inner_x as usize && col_no < inner_x_end as usize {
                    let inner_col_no = col_no - inner_x as usize;
                    let inner_row_no = row_no - inner_y as usize;
                    row.push(inner[inner_row_no][inner_col_no].clone());
                } else {
                    row.push(None);
                }
            }

            layer.push(row);
        }

        let mut new_cols : TetrisCols = HashMap::new();

        for (b, col) in self.settings.box_colors.iter() {
            let mut new_col = col.clone();
            new_col[3] = inner_alpha.clone() as f32;
            new_cols.insert(b.clone(), new_col);
        }

        self.draw_matrix(&layer, outer_pos, &new_cols, c, g);
    }

    /// Draw the game.
    pub fn draw<G:Graphics>(&self, ctlr : &TetrisController, c : &Context, g: &mut G) {
        // Display the next block
        self.draw_matrix(
            &ctlr.game.matrix_next,
            &self.settings.upcoming_position,
            &self.settings.box_colors,
            c, g
        );

        // Draw a black background if the game is over
        if ctlr.game.game_over {
            let rect = [ self.settings.main_position[0]
                       , self.settings.main_position[1]
                       , 10.0 * self.settings.box_size
                       , 22.0 * self.settings.box_size ];
            Rectangle::new(self.settings.game_over_col)
                .draw(rect, &c.draw_state, c.transform, g);
        }

        // Display the current stack
        self.draw_matrix(
            &ctlr.game.matrix_set,
            &self.settings.main_position, 
            &self.settings.box_colors,
            c, g
        );

        // Display the current block
        self.overlay_matrix(
            &ctlr.game.matrix_set,
            &self.settings.main_position,
            &ctlr.game.matrix_current,
            &ctlr.game.position_current,
            1.0, c, g
        );

        // Display the 'phantom' block
        self.overlay_matrix(
            &ctlr.game.matrix_set,
            &self.settings.main_position,
            &ctlr.game.matrix_current,
            &ctlr.game.lowest_pos(),
            0.4, c, g
        );
	}
}


