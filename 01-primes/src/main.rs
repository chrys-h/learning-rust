use std::collections::LinkedList;
use std::env::args;

/* Sieve of Eratosthenes */

struct Sieve {
    primes : LinkedList<u32>,
    latest : u32
}

fn init_sieve() -> Sieve {
    let mut list = LinkedList::new();
    list.push_front(2);
    Sieve { primes: list, latest: 2 }
}

fn test_n<'a, I>(mut primes : I, n : u32) -> bool
where
    I: Iterator<Item = &'a u32>,
{
    primes.all(|p| n % p != 0)
}

impl Iterator for Sieve {
    type Item = u32;
    fn next(&mut self) -> Option<u32> {
        let old_latest = self.latest;
        loop {
            self.latest += 1;
            if test_n(self.primes.iter(), self.latest) {
                self.primes.push_back(self.latest);
                break;
            }
        }
        Some(old_latest)
    }
}

fn test_init(i : u32) -> bool {
    let it : LinkedList<u32> = init_sieve()
        .take_while(|p| p * p <= i)
        .collect();
    test_n(it.iter(), i)
}


/* Commands */

fn cmd_first(n : u32) {
    let it = init_sieve().take(n as usize);
    for p in it {
        println!("{}", p);
    }
}

fn cmd_under(n: u32) {
    let it = init_sieve().take_while(|&p| p <= n);
    for p in it {
        println!("{}", p);
    }
}

fn cmd_factor(n: u32) {
    let it = init_sieve()
        .take_while(|&p| p <= n)
        .filter(|&p| n % p == 0);
    for p in it {
        println!("{}", p);
    }
}

fn cmd_test(n:u32) {
    if test_init(n) {
        println!("{} is prime", n);
    } else {
        println!("{} is not prime", n);
    }
}


/* Main function */

fn main() {
    let (cmd, param) : (String, u32) = if args().count() == 3 {
        (args().nth(1).expect(""),
         args().nth(2).expect("").parse::<u32>().unwrap())
    } else {
        (String::from("first"),
         args().nth(1).expect("").parse::<u32>().unwrap())
    };

    match cmd.as_ref() {
        "first"  => cmd_first(param),
        "under"  => cmd_under(param),
        "factor" => cmd_factor(param),
        "test"   => cmd_test(param),
        s => println!("Invalid command: {}", s)
    };
}


