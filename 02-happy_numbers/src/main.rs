use std::collections::HashMap;
use std::env::args;

/* Checking whether a number is happy */

struct HappyIterator {
    nums : HashMap<u32, bool>,
    latest : u32
}

fn init_happy() -> HappyIterator {
    let mut hm = HashMap::new();
    hm.insert(1, true);
    HappyIterator { latest: 1, nums: hm }
}

fn happy_transform(i : u32) -> u32 {
    let mut res : u32 = 0;
    let mut curr_i : u32 = i;
    while curr_i != 0 {
        res += (curr_i % 10).pow(2);
        curr_i = curr_i / 10;
    }

    res
}

fn test_n(happies : &mut HashMap<u32, bool>, i : u32) -> bool {
    let mem_res = happies.get(&i).cloned();
    match mem_res {
        Some(b) => b,
        None => {
            happies.insert(i, false);
            if test_n(happies, happy_transform(i)) {
                happies.insert(i, true);
                true
            } else {
                false
            }
        }
    }
}

impl Iterator for HappyIterator {
    type Item = u32;
    fn next(&mut self) -> Option<u32> {
        let old_latest = self.latest;
        loop {
            self.latest += 1;
            if test_n(&mut self.nums, self.latest) {
                break;
            }
        }
        Some(old_latest)
    }
}

/* Main function */

fn main() {
    let count : u32 = match args().nth(1) {
        Some(s) => s.parse::<u32>().unwrap(),
        None => 8
    };

    let it = init_happy().take(count as usize);
    for x in it {
        println!("{}", x);
    }
}

