# rt-crack

`rt-crack` is a hash cracker based on rainbow tables.

## Usage

As usual, the program can be compiled by running
```
$ cargo build
```

and the tests can be run with
```
$ cargo test
```

## Custom rainbow tables


