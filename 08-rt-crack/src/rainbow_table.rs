//! This module defines the high-level rainbow table functions

use std::path::Path;
use rusqlite::{Connection, Statement};
use std::io;
use hash_funs::hash_fun_by_name;
use reduce_funs::reduce;

// The tables are stored in files as SQLite databases with the following tables:
//      rainbow : seed(Varchar), hash(Blob)
//      params :  name(Varchar), value(Varchar)
// Rainbow contains the actual rainbow table. params contains the parameters:
//      "hash_fun" : the name of the hash function
//      "chain_length" : the length of each chain

/// Create all the tables in the database
fn initialize_db(conn : &mut Connection) {
    conn.execute_batch("BEGIN;
        CREATE TABLE rainbow (
            hash BLOB PRIMARY KEY,
            seed VARCHAR(24) NOT NULL UNIQUE
        );
        CREATE TABLE params (
            name VARCHAR(10) PRIMARY KEY,
            val VARCHAR(24) NOT NULL
        );
        COMMIT;").unwrap();
}

/// Set a parameter in the PARAMS table
fn set_param(conn : &mut Connection, name : &str, val : &String) {
    let n = &name.to_string();
    conn.execute("INSERT INTO params(name, value) VALUES (?1, ?2);", &[n, val]).unwrap();
}

/// Get a parameter from the PARAMS table
fn get_param(conn : &mut Connection, name : &str) -> String {
    let n = &name.to_string();
    conn.query_row("SELECT VALUE FROM params WHERE name = ?", &[n], |r| r.get(0))
        .unwrap()
}

/// Represents a rainbow table
struct RainbowTable<'a> {
    /// The SQLite database that contains the table
    conn : Connection,

    /// The hashing function
    hash_f : &'a Fn(&String) -> Vec<u8>,

    /// The length of each chain
    chain_length : u32,
}

impl<'a> RainbowTable<'a> {
    /// Load a table from a file
    pub fn from_file(path : &Path) -> Self {
        let mut conn = Connection::open(path).unwrap();
        let hash_f_name = get_param(&mut conn, "hash_f_name");
        let hash_f = hash_fun_by_name(&hash_f_name).unwrap();
        let chain_length = get_param(&mut conn, "chain_length").parse::<u32>().unwrap();

        RainbowTable { conn, hash_f, chain_length }
    }

    /// Create a new empty table in a file with the given parameters:
    ///     path         : the path to the file
    ///     hash_f_name  : the name of the hashing function (see hash_funs.rs)
    ///     chain_length : the length of the chains
    pub fn new(
        path : &Path,
        hash_f_name : &String,
        chain_length : u32
    ) -> Self {
        let hash_f = hash_fun_by_name(hash_f_name).unwrap();
        let mut conn = Connection::open(path).unwrap();
       
        initialize_db(&mut conn);
        set_param(&mut conn, "hash_f_name", &hash_f_name);
        set_param(&mut conn, "chain_length", &chain_length.to_string());

        RainbowTable { conn, hash_f, chain_length }
    }

    /// Generate chains into a table
    pub fn generate(&mut self, number : u32) {
        // Prepare the statement for speed
        let mut add_chain_stmt = self.conn
            .prepare("INSERT INTO rainbow(seed, hash) VALUES (?1, ?2);")
            .unwrap();

        // Add the requested number of chains
        for _ in 0..number {
            let (seed, hash) = self.generate_chain();
            add_chain_stmt.execute(&[&seed, &hash]).unwrap();
        }
    }

    /// Generate one chain
    fn generate_chain(&self) -> (String, Vec<u8>) {
        let seed = String::new();
        let mut hash = (self.hash_f)(&seed);

        for i in 0..self.chain_length - 1 {
            hash = (self.hash_f)(&reduce(i, hash));
        }

        (seed, hash)
    }

    /// Attempt to crack a hash with the table
    pub fn crack(&self, hash : &Vec<u8>) -> Option<String> {
        // Prepare the retrieve statement
        let mut get_chain_stmt = self.conn
            .prepare("SELECT SEED FROM RAINBOW WHERE HASH = ?;")
            .unwrap();
        
        let mut check_hash = hash.clone();

        // Look for the hash
        for i in (0..self.chain_length).rev() {
            let result = get_chain_stmt.query_row(&[&check_hash], |r| r.get(0));
            if let Ok(seed) = result {
                return Some(self.get_in_chain(&seed, &hash));
            }

            check_hash = (self.hash_f)(&reduce(i, &check_hash));
        }

        None
    }

    /// Find the cleartext of the given hash in the chain of the given seed.
    /// Fails if it is not found.
    fn get_in_chain(&self, seed : &String, hash : &Vec<u8>) -> String {
    }

    /// Look for a chain with the given hash as a final hash. Returns the
    /// seed of the chain if one is found.
    fn get_chain(&mut self, hash : &Vec<u8>) -> Option<String> {
        self.conn.query_row(, 
                &[hash], |r| r.get(0))
            .ok()
    }
}


