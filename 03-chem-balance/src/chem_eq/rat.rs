use std::ops::{Add, Sub, Neg, Mul, Div};
use std::cmp::Ordering;

fn gcd(i : i32, j : i32) -> i32 {
    if i == 0 {
        j
    } else {
        gcd(j % i, i)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Rat {
    num : i32,
    den : i32
}

impl Rat {
    pub fn numerator(&self) -> i32 { self.num }
    pub fn denominator(&self) -> i32 { self.den }

    pub fn new(n : i32, d : i32) -> Self {
        let cd = gcd(n, d);
        if cd == 0 {
            panic!("Zero denominator")
        } else {
            Rat { num: n / cd, den: d / cd }
        }
    }

    pub fn from_int(i : i32) -> Self {
        Rat { num : i, den : 1 }
    }

    pub fn is_zero(&self) -> bool {
        self.num == 0
    }

    pub fn zero() -> Self { Rat::from_int(0) }
    pub fn one() -> Self { Rat::from_int(1) }

    pub fn abs(&self) -> Self {
        let rat = self.clone();
        if rat < Rat::zero() {
            -rat
        } else {
            rat
        }
    }
}

impl Ord for Rat {
    fn cmp(&self, other : &Self) -> Ordering {
        let new_den_self = self.den * other.num;
        let new_den_other = other.den * self.num;

        if new_den_self < new_den_other {
            Ordering::Less
        } else if new_den_self > new_den_other {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    }
}

impl PartialOrd for Rat {
    fn partial_cmp(&self, other : &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Add for Rat {
    type Output = Self;

    fn add(self, other : Self) -> Self {
        let new_den = self.den * other.den;
        let new_num = self.num * other.den + other.num * self.den;
        Rat::new(new_num, new_den)
    }
}

impl Sub for Rat {
    type Output = Self;

    fn sub(self, other : Self) -> Self {
        let new_den = self.den * other.den;
        let new_num = self.num * other.den - other.num * self.den;
        Rat::new(new_num, new_den)
    }
}

impl Neg for Rat {
    type Output = Self;
    
    fn neg(self) -> Self {
        let new_den = -self.den;
        Rat::new(new_den, self.num)
    }
}

impl Mul for Rat {
    type Output = Self;

    fn mul(self, other : Self) -> Self {
        Rat::new(self.num * other.num, self.den * other.den)
    }
}

impl Div for Rat {
    type Output = Self;

    fn div(self, other : Self) -> Self {
        if other.is_zero() { panic!("Division by zero"); }
        Rat::new(self.num * other.den, self.den * other.num)
    }
}


