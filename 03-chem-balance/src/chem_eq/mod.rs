use std::collections::{BTreeSet, HashSet, HashMap};
use std::fmt;
use itertools::Itertools;

mod rat;
use self::rat::Rat;
mod rat_matrix;
use self::rat_matrix::RatMatrix;

#[derive(Clone, Debug)]
struct Molecule {
    conts : HashMap<String, i32>
}

fn mol_el_name((e, n) : (&String, &i32)) -> String {
    if n.clone() == 1 {
        e.clone()
    } else {
        format!("{}{}", e, n)
    }
}

impl Molecule {
    fn name(&self) -> String {
        self.conts.iter()
            .map(mol_el_name)
            .collect::<BTreeSet<String>>()
            .iter()
            .cloned()
            .collect()
    }
}

impl fmt::Display for Molecule {
    fn fmt(&self, f : &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

#[derive(Clone, Debug)]
struct ChemEqSide {
    mols : Vec<(Molecule, i32)>
}

impl ChemEqSide {
    fn all_atoms(&self) -> HashSet<String> {
        self.mols.iter()
            .map(|&(ref mol, _)| mol.conts.keys().cloned().collect::<HashSet<String>>())
            .fold(HashSet::new(), |res, curr| res.union(&curr).cloned().collect())
    }
}

fn mol_with_no(mol : &(Molecule, i32)) -> String {
    match mol {
        &(ref m, 1) => m.name(),
        &(ref m, n) => format!("{}{}", n, m.name())
    }
}

impl fmt::Display for ChemEqSide {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s : String = self.mols.iter()
            .map(mol_with_no)
            .intersperse(" + ".to_string())
            .collect();
        write!(f, "{}", s)
    }
}

#[derive(Clone, Debug)]
pub struct ChemEq {
    left_hand : ChemEqSide,
    right_hand : ChemEqSide
}

fn reduce_coefs(coefs : Vec<Rat>) -> Vec<i32> {
    Vec::new()
}

impl ChemEq {
    pub fn from_string(s : String) -> Self {
        parse_chem_eq(&s).unwrap().1
    }

    fn to_matrix(self) -> RatMatrix {
        RatMatrix::new_with_size(10, 10)
    }

    fn attrib_coefs(self, coefs : Vec<i32>) -> ChemEq {
        self
    }

    pub fn balance(self) -> Option<ChemEq> {
        let res = self.clone();
        self.to_matrix()
            .solve_linear()
            .map(reduce_coefs)
            .map(|v| res.attrib_coefs(v))
    }
}

impl fmt::Display for ChemEq {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} -> {}", self.left_hand, self.right_hand)
    }
}


/*************** Parser ****************/

named!(integer<&str, i32>,
    do_parse!(
        ds: many1!(complete!(one_of!("0123456789"))) >>
        (ds.iter().collect::<String>().parse::<i32>().unwrap())
    )
);

named!(parse_mol_elem<&str, (String, i32)>,
    do_parse!(
        l1: one_of!("ABCDEFGHIJKLMNOPQRSTUVWXYZ") >>
        ls: many0!(complete!(one_of!("abcdefghijklmnopqrstuvwxyz"))) >>
        n_opt: opt!(integer) >> 
        ((format!("{}{}", l1, ls.iter().collect::<String>()), 
          n_opt.unwrap_or(1)))
    )
);

named!(parse_molecule<&str, Molecule>,
    do_parse!(
        els: many1!(complete!(parse_mol_elem)) >>
        (Molecule{ conts: els.iter().cloned().collect() })
    )
);

fn add_no_num(mvec : Vec<Molecule>) -> ChemEqSide {
    let v = mvec.iter()
        .map(|ref m| ((*m).clone(), 1))
        .collect();
    ChemEqSide { mols : v }
}

named!(parse_chem_eq<&str, ChemEq>,
    do_parse!(
        lhs: separated_list!(ws!(tag_s!("+")), parse_molecule) >>
        ws!(tag_s!("->")) >>
        rhs: separated_list!(complete!(ws!(tag_s!("+"))), parse_molecule) >>
        (ChemEq{
            left_hand: add_no_num(lhs),
            right_hand: add_no_num(rhs)
        })
    )
);


