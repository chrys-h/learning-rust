use chem_eq::rat::Rat;

#[derive(Clone, Debug)]
pub struct RatMatrix {
    h : usize,
    w : usize,
    cs : Vec<Rat>
}

impl RatMatrix {
    pub fn width(&self)  -> usize { self.w }
    pub fn height(&self) -> usize { self.h }
    pub fn conts(&self)  -> &Vec<Rat> { &self.cs }

    fn index(&self, row : usize, col : usize) -> usize {
        col + self.h * row
    }

    pub fn new_with_size(height : usize, width : usize) -> Self {
        RatMatrix {
            h: height,
            w: width,
            cs: vec![Rat::zero(); height * width]
        }
    }

    pub fn get(&self, row : usize, col : usize) -> Rat {
        self.cs[self.index(row, col)].clone()
    }

    pub fn at(&self, row : usize, col : usize) -> Option<Rat> {
        if col < self.w && row < self.h {
            Some(self.get(row, col))
        } else { None }
    }

    pub fn set(&mut self, row : usize, col : usize, l : Rat) {
        let index = self.index(row, col);
        self.cs[index] = l;
    }

    pub fn rows(&self) -> RMRowIterator {
        RMRowIterator {
            matrix : &self,
            current : 0
        }
    }

    fn swap_rows(&mut self, r1 : usize, r2 : usize) {
        if r1 >= self.h || r2 >= self.h {
            panic!("Matrix row out of bounds in swap");
        } else if r1 != r2 {
            for col in 0..self.w - 1 {
                let i1 = self.index(col, r1);
                let i2 = self.index(col, r2);

                let x = self.cs[i1].clone();
                self.cs[i1] = self.cs[i2].clone();
                self.cs[i2] = x;
            }
        }
    }

    fn row_echelon(&self) -> Self {
        // Using gaussian elimination
        let mut res = self.clone();
        let mut row : usize = 0;
        let mut col : usize = 0;

        while row < res.h && col < res.w {
            let mut row_max = row;
            for i in row..res.h - 1 {
                if res.get(i, col).abs() > res.get(row_max, col).abs() {
                    row_max = i;
                }
            }

            let row_max = row_max;

            if res.get(row_max, col).is_zero() {
                col += 1;
            } else {
                res.swap_rows(row, row_max);

                for i in row_max + 1 .. res.h - 1 {
                    let f : Rat = res.get(i, col) / res.get(row, col);

                    let index = res.index(i, col);
                    res.cs[index] = Rat::zero();

                    for j in col + 1 .. res.w - 1 {
                        let index = res.index(i, j);
                        res.cs[index] = res.get(i, j) - res.get(row, j) * f.clone();
                    }
                }

                row += 1;
                col += 1;
            }
        }

        res
    }

    fn upside_down(&self) -> Self {
        let mut res = self.clone();
        let mut row = 0;

        while row * 2 < self.height() {
            res.swap_rows(row, self.height() - row);
        }

        res
    }

    pub fn solve_linear(&self) -> Option<Vec<Rat>> {
        let row_ech_form : RatMatrix = self.row_echelon();
        let mut res : Vec<Option<Rat>> = vec![None; self.height()];

        if row_ech_form.rows().all(eq_solvable) {
            for eq in row_ech_form.upside_down().rows() {
            }

            Some(res.iter().cloned().map(|o| o.unwrap()).collect())
        } else {
            None
        }
    }
}

fn eq_solvable(eq : &[Rat]) -> bool {
    let end = eq.len() - 2;
    if eq[0..end].iter().all(|r| r.is_zero()) {
        eq[eq.len() - 1].is_zero()
    } else {
        true
    }
}

struct RMRowIterator<'a> {
    matrix : &'a RatMatrix,
    current : usize
}

impl<'a> Iterator for RMRowIterator<'a> {
    type Item = &'a [Rat];

    fn next(&mut self) -> Option<&'a [Rat]> {
        self.current += 1;
        if self.current < self.matrix.height() {
            let start = (self.current - 1) * self.matrix.width();
            let end = (self.current * self.matrix.width()) - 1;
            Some(&self.matrix.conts()[start .. end])
        } else {
            None
        }
    }
}              







