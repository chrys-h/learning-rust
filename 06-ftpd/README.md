# Toy FTPD

I wrote this FTP server as a project to get familiar with the Rust programming language.

## TODO

- Actually implement settings

	- Implement configuration

	- Implement authentication (use actual UNIX users ?)

- More abstraction over data connection -> refactor transfer.rs

- Implement the remaining FTP commands, modes, structures

	- Management of per-connection working directories

- Filesystem abstraction

- Documentation

- Better logging

- Tests

- Connection timeout

- Daemonize server ?


