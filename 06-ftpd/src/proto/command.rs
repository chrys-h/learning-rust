use nom::{IResult, ErrorKind, digit};
use std::path::PathBuf;
use std::str::FromStr;
use std::net::{SocketAddr, IpAddr};

///////////////////////////////////////////////////////////////////////////////
//                          PROTOCOL DATA TYPES                              //
///////////////////////////////////////////////////////////////////////////////

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct HostPort {
    /// The IP address of the remote host
    pub ip_addr : String,

    /// The port to connect on the remote host for the
    /// data connection
    pub port : u16
}

impl HostPort {
    pub fn from_ftp(ints : Vec<u8>) -> Self {
        HostPort {
            ip_addr : format!("{}.{}.{}.{}", ints[0], ints[1], ints[2], ints[3]),
            port : ints[4] as u16 * 256 + ints[5] as u16
        }
    }

    pub fn to_socket_addr(&self) -> SocketAddr {
        SocketAddr::new(IpAddr::V4(self.ip_addr.parse().unwrap()), self.port)
    }
}


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum FormCode {
    NonPrint,
    Telnet,
    CarriageControl
}

impl FormCode {
}


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum TypeCode {
    Ascii(FormCode),
    Ebcdic(FormCode),
    Image,
    Local(u32)
}

impl TypeCode {
}


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum StructureCode {
    File,
    Record,
    Page
}

impl StructureCode {
}


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum ModeCode {
    Stream,
    Block,
    Compressed,
}

impl ModeCode {
}


#[derive(Debug, Eq, PartialEq)]
/// Possible FTP commands, taken from IETF RFC 959, in the order they are
/// presented in in section 5.3.1.
pub enum FtpCommand {
    User(String),
    Pass(String),
    Acct(String),
    Cwd(PathBuf),
    Cdup,
    Smnt(PathBuf),
    Quit,
    Rein,
    Port(HostPort),
    Pasv,
    Type(TypeCode),
    Stru(StructureCode),
    Mode(ModeCode),
    Retr(PathBuf),
    Stor(PathBuf),
    Stou,
    Appe(PathBuf),
    Allo(u32, Option<u32>),
    Rest(String),
    Rnfr(PathBuf),
    Rnto(PathBuf),
    Abor,
    Dele(PathBuf),
    Rmd(PathBuf),
    Mkd(PathBuf),
    Pwd,
    List(Option<PathBuf>),
    Nlst(Option<PathBuf>),
    Site(String),
    Syst,
    Stat(Option<PathBuf>),
    Help(Option<String>),
    Noop,
}

use self::FtpCommand::*;

impl FtpCommand {
    /// Parse an FTP command from text - TODO
    pub fn parse(buff : &str) -> IResult<&str, Self> {
        ftp_command(buff)
    }
}




///////////////////////////////////////////////////////////////////////////////
//                            PROTOCOL PARSER                                //
///////////////////////////////////////////////////////////////////////////////

named!(ftp_command<&str, FtpCommand>,
    alt!(
        user_command |
        pass_command |
        acct_command |
        cwd_command |
        cdup_command |
        smnt_command |
        quit_command |
        rein_command |
        port_command |
        pasv_command |
        type_command |
        stru_command |
        mode_command |
        retr_command |
        stor_command |
        stou_command |
        appe_command |
        allo_command |
        rest_command |
        rnfr_command |
        rnto_command |
        abor_command |
        dele_command |
        rmd_command |
        mkd_command |
        pwd_command |
        list_command |
        nlst_command |
        site_command |
        syst_command |
        stat_command |
        help_command |
        noop_command
    )
);

named!(ftp_string<&str, String>,
    do_parse!(
        s : take_till1!(|c| c == '\r') >>
        (s.to_string())
    )
);

named!(ftp_path<&str, PathBuf>,
    map!(ftp_string, PathBuf::from)
);

named!(user_command<&str, FtpCommand>,
    do_parse!(
        tag!("USER ") >>
        uname: ftp_string >>
        tag!("\r\n") >>
        (User(uname))
    )
);

named!(pass_command<&str, FtpCommand>,
    do_parse!(
        tag!("PASS ") >>
        pass: ftp_string >>
        tag!("\r\n") >>
        (Pass(pass))
    )
);

named!(acct_command<&str, FtpCommand>,
    do_parse!(
        tag!("ACCT ") >>
        info: ftp_string >>
        tag!("\r\n") >>
        (Acct(info))
    )
);

named!(cwd_command<&str, FtpCommand>,
    do_parse!(
        tag!("CWD ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Cwd(path))
    )
);

named!(cdup_command<&str, FtpCommand>,
    do_parse!(
        tag!("CDUP\r\n") >>
        (Cdup)
    )
);

named!(smnt_command<&str, FtpCommand>,
    do_parse!(
        tag!("SMNT ") >>
        path : ftp_path >>
        (Smnt(path))
    )
);

named!(quit_command<&str, FtpCommand>,
    do_parse!(
        tag!("QUIT\r\n") >>
        (Quit)
    )
);

named!(rein_command<&str, FtpCommand>,
    do_parse!(
        tag!("REIN\r\n") >>
        (Rein)
    )
);

named!(ftp_int<&str, u32>,
    map_res!(digit, FromStr::from_str)
);

named!(ftp_int8<&str, u8>,
    map_res!(digit, FromStr::from_str)
);

named!(host_port<&str, HostPort>,
    map!(
        separated_list!(
            tag!(","),
            ftp_int8),
        HostPort::from_ftp
    )
);

named!(port_command<&str, FtpCommand>,
    do_parse!(
        tag!("PORT ") >>
        p : host_port >>
        tag!("\r\n") >>
        (Port(p))
    )
);

named!(pasv_command<&str, FtpCommand>,
    do_parse!(
        tag!("PASV\r\n") >>
        (Pasv)
    )
);

named!(form_code<&str, FormCode>,
    alt!(
        do_parse!(tag!("N") >> (FormCode::NonPrint)) |
        do_parse!(tag!("T") >> (FormCode::Telnet)) |
        do_parse!(tag!("C") >> (FormCode::CarriageControl))
    )
);

named!(type_code_ascii<&str, TypeCode>,
    do_parse!(
        tag!("A") >>
        opt_fcode : opt!(
            preceded!(
                tag!(" "),
                form_code
            )) >> 
        (TypeCode::Ascii(opt_fcode.unwrap_or(FormCode::NonPrint)))
    )
);

named!(type_code_ebcdic<&str, TypeCode>,
    do_parse!(
        tag!("E") >>
        opt_fcode : opt!(
            preceded!(
                tag!(" "),
                form_code
            )) >> 
        (TypeCode::Ebcdic(opt_fcode.unwrap_or(FormCode::NonPrint)))
    )
);

named!(type_code_image<&str, TypeCode>,
    do_parse!(
        tag!("I") >>
        (TypeCode::Image)
    )
);

named!(type_code_local<&str, TypeCode>,
    do_parse!(
        tag!("L ") >>
        sz : ftp_int >>
        (TypeCode::Local(sz))
    )
);

named!(type_code<&str, TypeCode>,
    alt!(type_code_ascii | type_code_ebcdic | type_code_image | type_code_local)
);

named!(type_command<&str, FtpCommand>,
    do_parse!(
        tag!("TYPE ") >>
        code : type_code >>
        tag!("\r\n") >>
        (Type(code))
    )
);

named!(structure_code<&str, StructureCode>,
    alt!(
        do_parse!(tag!("F") >> (StructureCode::File)) |
        do_parse!(tag!("R") >> (StructureCode::Record)) |
        do_parse!(tag!("P") >> (StructureCode::Page))
    )
);

named!(stru_command<&str, FtpCommand>,
    do_parse!(
        tag!("STRU ") >>
        code : structure_code >>
        tag!("\r\n") >>
        (Stru(code))
    )
);

named!(mode_code<&str, ModeCode>,
    alt!(
        do_parse!(tag!("S") >> (ModeCode::Stream)) |
        do_parse!(tag!("B") >> (ModeCode::Block)) |
        do_parse!(tag!("C") >> (ModeCode::Compressed))
    )
);

named!(mode_command<&str, FtpCommand>,
    do_parse!(
        tag!("MODE ") >>
        code : mode_code >>
        tag!("\r\n") >>
        (Mode(code))
    )
);

named!(retr_command<&str, FtpCommand>,
    do_parse!(
        tag!("RETR ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Retr(path))
    )
);

named!(stor_command<&str, FtpCommand>,
    do_parse!(
        tag!("STOR ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Stor(path))
    )
);

named!(stou_command<&str, FtpCommand>,
    do_parse!(
        tag!("STOP\r\n") >>
        (Stou)
    )
);

named!(appe_command<&str, FtpCommand>,
    do_parse!(
        tag!("APPE ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Appe(path))
    )
);

named!(allo_command<&str, FtpCommand>,
    do_parse!(
        tag!("ALLO ") >>
        // TODO
        tag!("\r\n") >>
        (Allo(0, None))
    )
);

named!(ftp_pr_string<&str, String>,
    do_parse!(
        // TODO
        (String::new())
    )
);

named!(rest_command<&str, FtpCommand>,
    do_parse!(
        tag!("REST ") >>
        marker : ftp_pr_string >>
        tag!("\r\n") >>
        (Rest(marker))
    )
);

named!(rnfr_command<&str, FtpCommand>,
    do_parse!(
        tag!("RNFR ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Rnfr(path))
    )
);

named!(rnto_command<&str, FtpCommand>,
    do_parse!(
        tag!("RNTO ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Rnto(path))
    )
);

named!(abor_command<&str, FtpCommand>,
    do_parse!(
        tag!("ABOR\r\n") >>
        (Abor)
    )
);

named!(dele_command<&str, FtpCommand>,
    do_parse!(
        tag!("DELE ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Dele(path))
    )
);

named!(rmd_command<&str, FtpCommand>,
    do_parse!(
        tag!("RMD ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Rmd(path))
    )
);

named!(mkd_command<&str, FtpCommand>,
    do_parse!(
        tag!("MKD ") >>
        path : ftp_path >>
        tag!("\r\n") >>
        (Mkd(path))
    )
);

named!(pwd_command<&str, FtpCommand>,
    do_parse!(
        tag!("PWD\r\n") >>
        (Pwd)
    )
);

named!(list_command<&str, FtpCommand>,
    do_parse!(
        tag!("LIST") >>
        opt_path : opt!(
            preceded!(
                tag!(" "),
                ftp_path
            )) >>
        tag!("\r\n") >>
        (List(opt_path))
    )
);

named!(nlst_command<&str, FtpCommand>,
    do_parse!(
        tag!("LIST") >>
        opt_path : opt!(
            preceded!(
                tag!(" "),
                ftp_path
            )) >>
        tag!("\r\n") >>
        (Nlst(opt_path))
    )
);

named!(site_command<&str, FtpCommand>,
    do_parse!(
        tag!("SITE ") >>
        s : ftp_string >>
        tag!("\r\n") >>
        (Site(s))
    )
);

named!(syst_command<&str, FtpCommand>,
    do_parse!(
        tag!("SYST\r\n") >>
        (Syst)
    )
);

named!(stat_command<&str, FtpCommand>,
    do_parse!(
        tag!("STAT") >>
        opt_path : opt!(
            preceded!(
                tag!(" "),
                ftp_path
            )) >>
        tag!("\r\n") >>
        (Stat(opt_path))
    )
);

named!(help_command<&str, FtpCommand>,
    do_parse!(
        tag!("HELP") >>
        opt_arg : opt!(
            preceded!(
                tag!(" "),
                ftp_string
            )) >>
        tag!("\r\n") >>
        (Help(opt_arg))
    )
);

named!(noop_command<&str, FtpCommand>,
    do_parse!(
        tag!("NOOP\r\n") >>
        (Noop)
    )
);



