use std::path::PathBuf;
use std::fs::{File, remove_file, copy, read_dir};
use std::env::temp_dir;

use transfer::TrPrepFailure;

/// Create a backup if a file is going to be overwritten
pub fn make_backup(path : &PathBuf) -> Result<Option<PathBuf>, TrPrepFailure> {
    if ! path.is_file() {
        Ok(None)
    } else {
        let mut backup_path = temp_dir();
        backup_path.push(path.file_name().unwrap());

        match copy(path, &backup_path) {
            Err(_) => Err(TrPrepFailure::NoFile),
            Ok(_)  => Ok(Some(backup_path))
        }
    }
}

/// Delete the file and restore the backup, if any.
#[allow(unused_must_use)]
pub fn restore_file(original_path : &PathBuf, backup : &Option<PathBuf>) {
    remove_file(original_path);
    if let &Some(ref backup_path) = backup {
        copy(backup_path, original_path);
        remove_file(backup_path);
    }
}

/// Delete the backup
#[allow(unused_must_use)]
pub fn cleanup_backup(backup : &Option<PathBuf>) {
    if let &Some(ref path) = backup {
        remove_file(path);
    }
}

/// Attempt to open an existing file
pub fn get_open_file(path : &PathBuf) -> Result<File, TrPrepFailure> {
    match File::open(path) {
        Ok(file) => Ok(file),
        Err(_)   => Err(TrPrepFailure::NoFile),
    }
}

/// Attempt to create (and possibly overwrite) a file
pub fn get_new_file(path : &PathBuf) -> Result<File, TrPrepFailure> {
    match File::create(path) {
        Ok(file) => Ok(file),
        Err(_)   => Err(TrPrepFailure::NoFile),
    }
}

/// List files in a given directory, one file per line
pub fn ls_files(path : &PathBuf) -> Result<String, TrPrepFailure> {
    let ls = read_dir(path);
    if ls.is_err() { return Err(TrPrepFailure::NoFile); }
    let ls = ls.unwrap();
    let mut list : String = String::new();

    for entry in ls {
        if entry.is_err() { return Err(TrPrepFailure::NoFile); }
        let entry_path = entry.unwrap().path();
        let name = match entry_path.file_name() {
            Some(n) => n,
            None => return Err(TrPrepFailure::LocalError),
        };
        list.push_str(name.to_str().unwrap());
        if entry_path.is_dir() { list.push('/'); }
        list.push_str("\n");
    }

    Ok(list)
}

