/// Represents the settings of the server
#[derive(Debug)]
pub struct Settings {
}

impl Settings {
    /// Check whether a username belongs to an existing account
    pub fn user_exists(&self, username : &String) -> bool {
        username == "josefk"
    }

    /// Check username and password ; does not take anonymous login into
    /// account.
    pub fn authentify_user_pass(&self, username : &String, passwd : &String) -> bool {
        username == "josefk" && passwd == "GlassPerlenSpiel"
    }
}

