#![recursion_limit="128"]
#[macro_use] extern crate nom;

mod server;
use server::Server;
mod settings;
use settings::Settings;
mod proto;
mod transfer;
mod files;

static GLOBAL_SETTINGS : Settings = Settings {};

fn main() {
    Server::new("127.0.0.1:2121", &GLOBAL_SETTINGS).run();
}


