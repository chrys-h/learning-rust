use std::net::TcpListener;
use std::thread;

mod connection;
mod commands;
use self::connection::Connection;
use settings::Settings;

#[derive(Debug)]
pub struct Server {
    /// The listener socket
    listener : TcpListener,

    /// The global settings of the server
    settings : &'static Settings,
}

impl Server {
    /// Initialize a Server
    pub fn new(addr : &str, s : &'static Settings) -> Self {
        Server {
            listener: TcpListener::bind(addr).unwrap(),
            settings: s,
        }
    }

    /// Main listening loop: listen on the listening socket and create
    /// a new thread for each connection.
    pub fn run(&mut self) {
        loop {
            match self.listener.accept() {
                Ok((sock, addr)) => {
                    let st : &'static Settings = self.settings;
                    thread::spawn(move || {
                        Connection::handle(sock, addr, st);
                    });
                },
                Err(e) => {
                    println!("Could not accept on listener socket: {}", e);
                    break;
                }
            }
        }
    }
}


