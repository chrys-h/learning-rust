use std::path::PathBuf;

use proto::{HostPort, TypeCode, ModeCode, StructureCode, FormCode};
use server::connection::*;
use server::connection::ConnAuth::*;
use transfer::*;
use files::ls_files;
use std::env::current_dir;

impl Connection {
    /****************** Minimum implementation commands ******************/

    pub fn cmd_user(&mut self, uname : String) {
        if self.auth != Unauthenticated {
            self.bad_sequence();

        } else {
            self.auth = AwaitingPassword(uname);
            self.status("331 Enter password");
        }
    }

    pub fn cmd_quit(&mut self) {
        self.end_cleanly();
    }

    pub fn cmd_port(&mut self, host_port : HostPort) {
        self.transfer_settings.set_target(host_port.to_socket_addr());
        self.command_okay();
    }

    pub fn cmd_type(&mut self, type_code : TypeCode) {
        if type_code != TypeCode::Ascii(FormCode::NonPrint) && type_code != TypeCode::Image {
            self.cmd_not_implemented();
        } else {
            self.transfer_settings.set_type(type_code);
            self.command_okay();
        }
    }

    pub fn cmd_mode(&mut self, mode_code : ModeCode) {
        if mode_code != ModeCode::Stream {
            self.cmd_not_implemented();
        } else {
            self.transfer_settings.set_mode(mode_code);
            self.command_okay();
        }
    }

    pub fn cmd_stru(&mut self, stru_code : StructureCode) {
        if stru_code != StructureCode::File {
            self.cmd_not_implemented();
        } else {
            self.transfer_settings.set_structure(stru_code);
            self.command_okay();
        }
    }

    pub fn cmd_retr(&mut self, path : PathBuf) {
        if ! self.auth.authenticated() {
            self.status("530 Please log in");
            return;
        }

        let mut tr = match TransferTo::prepare(path, &self.transfer_settings) {
            Ok(res) => {
                self.status("150 File OK, about to open data connection");
                res
            },

            Err(er) => {
                self.status(er.message());
                return;
            },
        };

        match tr.transfer() {
            Ok(()) => self.status("226 Transfer successful"),
            Err(er) => self.status(er.message())
        };
    }

    pub fn cmd_stor(&mut self, path : PathBuf) {
        if ! self.auth.authenticated() {
            self.status("530 Please log in");
            return;
        }

        let mut tr = match TransferFrom::prepare(path, &self.transfer_settings) {
            Ok(res) => {
                self.status("150 File OK, about to open data connection");
                res
            },

            Err(er) => {
                self.status(er.message());
                return;
            },
        };

        match tr.transfer() {
            Ok(()) => self.status("226 Transfer successful"),
            Err(er) => self.status(er.message())
        };
    }

    pub fn cmd_noop(&mut self) {
        self.command_okay();
    }

    /****************** Additional commands ******************/

    pub fn cmd_pass(&mut self, passwd : String) {
        if ! self.auth.awaiting_password() {
            self.bad_sequence();

        } else if ! self.settings.authentify_user_pass(&self.auth.username(), &passwd) {
            self.status("530 Bad username or password");

        } else {
            let uname = self.auth.username().clone();
            self.auth = Authenticated(uname);
            self.status("230 Logged in, proceed");
        }
    }

    pub fn cmd_list(&mut self, path : Option<PathBuf>) {
        if ! self.auth.authenticated() {
            self.status("530 Please log in");
            return;
        };

        let listing = match ls_files(&path.unwrap_or(current_dir().unwrap())) {
            Ok(res) => res,
            Err(er) => {
                self.status(er.message());
                return;
            }
        };

        let mut tr = match TransferString::prepare(listing, &self.transfer_settings) {
            Ok(res) => {
                self.status("150 About to open data connection");
                res
            },

            Err(er) => {
                self.status(er.message());
                return;
            }
        };

        match tr.transfer() {
            Ok(()) => self.status("226 Transfer succesful"),
            Err(er) => self.status(er.message()),
        };
    }
}

