use std::net::{TcpStream, SocketAddr};
use nom::{IResult, Err};
use std::io::{Read, Write};

use settings::{Settings};
use proto::*;
use transfer::TransferSettings;

/// Represents the authentication state of the connection
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum ConnAuth {
    /// Session awaiting authentification
    Unauthenticated,

    /// Username given, awaiting password
    AwaitingPassword(String),

    /// Session authenticated as user
    Authenticated(String),
}

use self::ConnAuth::*;

impl ConnAuth {
    pub fn awaiting_password(&self) -> bool {
        match *self {
            AwaitingPassword(_) => true,
            _ => false
        }
    }

    pub fn authenticated(&self) -> bool {
        match *self {
            Authenticated(_) => true,
            _ => false
        }
    }

    pub fn username(&self) -> String {
        match *self {
            Authenticated(ref n) => n.clone(),
            AwaitingPassword(ref n) => n.clone(),
            Unauthenticated => panic!("Not authenticated")
        }
    }
}

#[derive(Debug)]
pub struct Connection {
    /// The communication socket
    pub sock : TcpStream,

    /// The address of the client
    pub client_addr : SocketAddr,

    /// The settings of the server
    pub settings : &'static Settings,

    /// The authentication status of the session
    pub auth : ConnAuth,

    /// The buffer for client input
    pub in_buffer : String,

    /// Whether the connection is supposed to continue after the
    /// latest command. Switch to false to end the connection cleanly.
    pub continue_existing : bool,

    /// Settings for the data connection
    pub transfer_settings : TransferSettings,
}

impl Connection {
    /// Construct a new session
    pub fn new(s : TcpStream, a : SocketAddr, st : &'static Settings) -> Self {
        Connection {
            sock : s,
            client_addr : a,
            settings : st,
            auth : ConnAuth::Unauthenticated,
            in_buffer : String::new(),
            continue_existing : true,
            transfer_settings : TransferSettings::new(a)
        }
    }

    /// Read data from the socket
    fn get_data(&mut self) {
        let mut buff = [0;255];
        match self.sock.read(&mut buff) {
            Ok(0) =>
                panic!("Control connection closed"),
            Err(e) =>
                panic!("Network error: {}", e),
            Ok(ln) => {
                let data = buff.iter().take(ln).cloned().collect();
                self.in_buffer.push_str(&String::from_utf8(data).unwrap());
            },
        }
    }

    /// Wait for a command
    fn next_command(&mut self) -> Result<FtpCommand, String> {
        if self.in_buffer.is_empty() {
            self.get_data();
        }

        let buf = self.in_buffer.clone();
        println!("Got data; Input buffer : {:?}", buf);
        let res = FtpCommand::parse(&buf);

        if is_incomplete(&res) {
            // I know this is ugly but that's the only solution I found to a
            // borrow checker issue. Don't judge me.
            self.get_data();
            self.next_command()
        } else {
            match res {
                Ok((r, x)) => {
                    self.in_buffer = r.to_string();
                    Ok(x)
                },
                Err(_) => {
                    self.in_buffer = String::new();
                    Err("500 Command unrecognized".to_string())
                },
            }
        }
    }

    /// Send and FTP reply
    pub fn status<S>(&mut self, s : S) where S: ToString {
        self.sock.write_all(format!("{}\r\n", s.to_string()).as_bytes())
            .expect("Could not write reply");
    }

    /// Handle a TCP connection
    pub fn handle(s : TcpStream, a : SocketAddr, st : &'static Settings) {
        println!("Initiating connection with {}", a);
        let mut conn = Self::new(s, a, st);
        conn.status("220 (Toy ftpd)");
        conn.run();
    }

    /// Set the connection to end cleanly. The connection will end right after
    /// the processing of the current command, before any new command is
    /// processed.
    pub fn end_cleanly(&mut self) {
        self.continue_existing = false;
    }

    /// Cleanup routine for the connection
    fn cleanup_and_die(&mut self) {
        self.status("221 Closing control connection.");
    }

    /// Main protocol loop
    fn run(&mut self) {
        while self.continue_existing {
            match self.next_command() {
                Ok(c) => self.run_command(c),
                Err(e) => self.status(e)
            };
        }

        self.cleanup_and_die();
    }

    /// Command dispatching. The commands are implemented in
    /// server/commands.rs
    fn run_command(&mut self, cmd : FtpCommand) {
        match cmd {
            FtpCommand::User(x)    => self.cmd_user(x),
            FtpCommand::Pass(x)    => self.cmd_pass(x),
            FtpCommand::Acct(_)    => self.cmd_not_implemented(),
            FtpCommand::Cwd(_)     => self.cmd_not_implemented(),
            FtpCommand::Cdup       => self.cmd_not_implemented(),
            FtpCommand::Smnt(_)    => self.cmd_not_implemented(),
            FtpCommand::Quit       => self.cmd_quit(),
            FtpCommand::Rein       => self.cmd_not_implemented(),
            FtpCommand::Port(x)    => self.cmd_port(x),
            FtpCommand::Pasv       => self.cmd_not_implemented(),
            FtpCommand::Type(x)    => self.cmd_type(x),
            FtpCommand::Stru(x)    => self.cmd_stru(x),
            FtpCommand::Mode(x)    => self.cmd_mode(x),
            FtpCommand::Retr(x)    => self.cmd_retr(x),
            FtpCommand::Stor(x)    => self.cmd_stor(x),
            FtpCommand::Stou       => self.cmd_not_implemented(),
            FtpCommand::Appe(_)    => self.cmd_not_implemented(),
            FtpCommand::Allo(_, _) => self.cmd_not_implemented(),
            FtpCommand::Rest(_)    => self.cmd_not_implemented(),
            FtpCommand::Rnfr(_)    => self.cmd_not_implemented(),
            FtpCommand::Rnto(_)    => self.cmd_not_implemented(),
            FtpCommand::Abor       => self.cmd_not_implemented(),
            FtpCommand::Dele(_)    => self.cmd_not_implemented(),
            FtpCommand::Rmd(_)     => self.cmd_not_implemented(),
            FtpCommand::Mkd(_)     => self.cmd_not_implemented(),
            FtpCommand::Pwd        => self.cmd_not_implemented(),
            FtpCommand::List(x)    => self.cmd_list(x),
            FtpCommand::Nlst(_)    => self.cmd_not_implemented(),
            FtpCommand::Site(_)    => self.cmd_not_implemented(),
            FtpCommand::Syst       => self.cmd_not_implemented(),
            FtpCommand::Stat(_)    => self.cmd_not_implemented(),
            FtpCommand::Help(_)    => self.cmd_not_implemented(),
            FtpCommand::Noop       => self.cmd_noop(),
        }
    }

    /// Send a "command not implemented" reply
    pub fn cmd_not_implemented(&mut self) {
        self.status("502 Commmand not implemented");
    }

    /// Send a "command OK" reply
    pub fn command_okay(&mut self) {
        self.status("200 Okay");
    }

    /// Send a "bad sequence" reply
    pub fn bad_sequence(&mut self) {
        self.status("503 Bad sequence");
    }
}

/// Helper function
fn is_incomplete<I, O>(ir : &IResult<I, O>) -> bool {
    match ir {
        &Err(Err::Incomplete(_)) => true,
        _ => false,
    }
}

