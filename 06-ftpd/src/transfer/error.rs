/// The possible reasons for the failure of a transfer preparation
#[derive(Debug, Clone)]
pub enum TrPrepFailure {
    NoFile,
    NoDataConn,
    LocalError,
}

use self::TrPrepFailure::*;

impl TrPrepFailure {
    /// Get the status message asociated with the error
    pub fn message(self) -> &'static str {
        match self {
            NoFile => "450 File not available",
            NoDataConn => "425 Can't open data connection",
            LocalError => "451 Local error in processing",
        }
    }
}

/// The possible reasons for the failure of a transfer
#[derive(Debug, Clone)]
pub enum TrFailure {
    NetworkFailure,
    StorageFailure,
}

use self::TrFailure::*;

impl TrFailure {
    /// Get the status message asociated with the error
    pub fn message(self) -> &'static str {
        match self {
            NetworkFailure => "426 Connection closed, transfer aborted",
            StorageFailure => "451 Local error in processing",
        }
    }
}


