mod error;
pub use self::error::*;
mod settings;
pub use self::settings::*;
use files::*;

use std::net::TcpStream;
use std::path::PathBuf;
use std::fs::File;
use std::io::{BufReader, BufRead, Write, Read};

use proto::{TypeCode, ModeCode, StructureCode, FormCode};

/// A transfer of a file to a remote host
#[derive(Debug)]
pub struct TransferTo {
    /// The settings of the data connection
    transfer_settings : TransferSettings,

    /// The handle to the open file
    file : File,

    /// The handle to the data connection socket
    data_socket : TcpStream,
}

impl TransferTo {
    /// Attempt to open the connection and the file for the transfer
    pub fn prepare(path : PathBuf, settings : &TransferSettings) -> Result<Self, TrPrepFailure> {
        let file = get_open_file(&path)?;
        let data_conn = settings.get_data_conn()?;

        Ok(TransferTo {
            transfer_settings : settings.clone(),
            data_socket : data_conn,
            file : file,
        })
    }

    /// Attempt to transfer the file
    pub fn transfer(&mut self) -> Result<(), TrFailure> {
        let mut transfer_status : Result<(), TrFailure> = Ok(());

        if self.transfer_settings.conn_type() == TypeCode::Ascii(FormCode::NonPrint) {
            for line_res in BufReader::new(&mut self.file).lines() {
                if let Ok(line) = line_res {
                    let buffer = line.trim_right_matches("\r\n");
                    if write!(self.data_socket, "{}\r\n", buffer).is_err() {
                        transfer_status = Err(TrFailure::NetworkFailure);
                    }

                } else {
                    transfer_status = Err(TrFailure::StorageFailure);
                }

                if transfer_status.is_err() { break; }
            }

        } else if self.transfer_settings.conn_type() == TypeCode::Image {
            let mut buffer = [0; 1024];

            loop {
                if let Ok(sz) = self.file.read(&mut buffer) {
                    if sz == 0 { break; }

                    if self.data_socket.write_all(&buffer[..sz]).is_err() {
                        transfer_status = Err(TrFailure::NetworkFailure);
                    }
                } else {
                    transfer_status = Err(TrFailure::StorageFailure);
                }

                if transfer_status.is_err() { break; }
            }
        }

        transfer_status
    }
}


/// A transfer of a file from a remote host
#[derive(Debug)]
pub struct TransferFrom {
    /// The settings of the data connection
    transfer_settings : TransferSettings,

    /// The handle to the open file
    file : File,

    /// The handle to the data connection socket
    data_socket : TcpStream,

    /// The filename of the backup copy of the file (if any)
    backup_filename : Option<PathBuf>,

    /// The filename of the file
    origin_filename : PathBuf,
}

impl TransferFrom {
    /// Attempt to open the connection and the file for the transfer
    pub fn prepare(path : PathBuf, settings : &TransferSettings) -> Result<Self, TrPrepFailure> {
        let backup = make_backup(&path)?;
        let file = get_new_file(&path)?;
        match settings.get_data_conn() {
            Err(e) => {
                restore_file(&path, &backup);
                Err(e)
            },

            Ok(conn) => {
                Ok(TransferFrom {
                    transfer_settings : settings.clone(),
                    data_socket : conn,
                    file : file,
                    origin_filename : path,
                    backup_filename : backup,
                })
            }
        }
    }

    /// Attempt to transfer the file
    pub fn transfer(&mut self) -> Result<(), TrFailure> {
        let mut transfer_status : Result<(), TrFailure> = Ok(());

        if self.transfer_settings.conn_type() == TypeCode::Ascii(FormCode::NonPrint) {
            for line_res in BufReader::new(&mut self.data_socket).lines() {
                if let Ok(line) = line_res {
                    let buffer = line.trim_right_matches("\r\n");
                    if write!(self.file, "{}\n", buffer).is_err() {
                        transfer_status = Err(TrFailure::NetworkFailure);
                    }

                } else {
                    transfer_status = Err(TrFailure::StorageFailure);
                }

                if transfer_status.is_err() { break; }
            }

        } else if self.transfer_settings.conn_type() == TypeCode::Image {
            let mut buffer = [0; 1024];

            loop {
                if let Ok(sz) = self.data_socket.read(&mut buffer) {
                    if sz == 0 { break; }

                    if self.file.write_all(&buffer[..sz]).is_err() {
                        transfer_status = Err(TrFailure::StorageFailure);
                    }
                } else {
                    transfer_status = Err(TrFailure::NetworkFailure);
                }

                if transfer_status.is_err() { break; }
            }
        }

        if transfer_status.is_err() {
            restore_file(&self.origin_filename, &self.backup_filename);
        } else {
            cleanup_backup(&self.backup_filename);
        }

        transfer_status
    }
}


/// A transfer of a string from the server to the client
#[derive(Debug)]
pub struct TransferString {
    /// The settings of the data connection
    transfer_settings : TransferSettings,

    /// The string to be transferred
    string : String,

    /// The handle to the data connection socket
    data_socket : TcpStream,
}

impl TransferString {
    /// Open the data connection
    pub fn prepare(s : String, settings : &TransferSettings) -> Result<Self, TrPrepFailure> {
        let data_conn = settings.get_data_conn()?;

        Ok(TransferString {
            transfer_settings : settings.clone(),
            data_socket : data_conn,
            string : s
        })
    }

    /// Run the actual transfer
    pub fn transfer(&mut self) -> Result<(), TrFailure> {
        let mut transfer_status : Result<(), TrFailure> = Ok(());

        for line_res in BufReader::new(&mut self.string[..].as_bytes()).lines() {
            if let Ok(line) = line_res {
                let buffer = line.trim_right_matches("\r\n");
                if write!(self.data_socket, "{}\r\n", buffer).is_err() {
                    transfer_status = Err(TrFailure::NetworkFailure);
                }

            } else {
                transfer_status = Err(TrFailure::StorageFailure);
            }

            if transfer_status.is_err() { break; }
        }

        transfer_status
    }
}



