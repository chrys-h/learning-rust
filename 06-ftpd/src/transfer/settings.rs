use std::net::{TcpStream, SocketAddr};

use transfer::error::TrPrepFailure;
use proto::*;

/// Settings for a data connection
#[derive(Debug, Clone)]
pub struct TransferSettings {
    /// The data structure for the data connection
    structure : StructureCode,

    /// Data type for the data connection
    conn_type : TypeCode,

    /// Data mode for the data connection
    mode : ModeCode,

    /// The connection target for data connection
    target : SocketAddr,
}

impl TransferSettings {
    /// Create a new instance
    pub fn new(sa : SocketAddr) -> Self {
        TransferSettings {
            conn_type : TypeCode::Ascii(FormCode::NonPrint),
            mode : ModeCode::Stream,
            target : sa,
            structure : StructureCode::File,
        }
    }

    pub fn set_mode(&mut self, n_mode : ModeCode) { self.mode = n_mode; }
    pub fn set_target(&mut self, n_target : SocketAddr) { self.target = n_target; }
    pub fn set_structure(&mut self, n_structure : StructureCode) { self.structure = n_structure; }
    pub fn set_type(&mut self, n_type : TypeCode) { self.conn_type = n_type; }

    pub fn mode(&self) -> ModeCode { self.mode.clone() }
    pub fn target(&self) -> SocketAddr { self.target.clone() }
    pub fn structure(&self) -> StructureCode { self.structure.clone() }
    pub fn conn_type(&self) -> TypeCode { self.conn_type.clone() }

    /// Attempt to open a data connexion
    pub fn get_data_conn(&self) -> Result<TcpStream, TrPrepFailure> {
        match TcpStream::connect(self.target) {
            Ok(sock) => Ok(sock),
            Err(_)   => Err(TrPrepFailure::NoDataConn)
        }
    }
}



